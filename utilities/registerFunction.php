<?php

if (session_id() == ""){
     session_start();
 }
 
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerNewUser($conn,$uid,$username,$email,$finalPassword,$salt,$phoneNo,$tac)
{
     if(insertDynamicData($conn,"user",array("uid","username","email","password","salt","phone_no","tac"),
          array($uid,$username,$email,$finalPassword,$salt,$phoneNo,$tac),"sssssss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $username = rewrite($_POST['register_username']);
     $email = rewrite($_POST['register_email_user']);
     $register_password = rewrite($_POST['register_password']);
     $register_password_validation = strlen($register_password);
     $register_retype_password = rewrite($_POST['register_retype_password']);
     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);
     $phoneNo = rewrite($_POST['register_phone']);
     $tac = rewrite($_POST['register_tac']);

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $username."<br>";
     // echo $email."<br>";
     // echo $finalPassword."<br>";
     // echo $salt."<br>";
     // echo $phoneNo ."<br>";
     // echo $tac."<br>";

     if($register_password == $register_retype_password)
     {
          if($register_password_validation >= 6)
          {
               $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
               $usernameDetails = $usernameRows[0];

               $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($_POST['register_email_user']),"s");
               $userEmailDetails = $userEmailRows[0];

               $userPhoneRows = getUser($conn," WHERE phone_no = ? ",array("phone_no"),array($_POST['register_phone']),"s");
               $userPhoneDetails = $userPhoneRows[0];

               if(!$userEmailDetails && !$userPhoneDetails && !$usernameDetails)
               {
                    if(registerNewUser($conn,$uid,$username,$email,$finalPassword,$salt,$phoneNo,$tac))
                    {
                         $_SESSION['messageType'] = 1;
                         header('Location: ../profile.php?type=1');
                         // echo "done register";   
                         // $_SESSION['uid'] = $uid;
                         // echo "<script>alert('Register Success Without Upline!');window.location='../userDashboard.php'</script>";

                    }
               }
               else
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../index.php?type=5');
                    //echo "fail to register register";
                    //echo "<script>alert('register details has been used by others');window.location='../addUser.php'</script>";
               }
          }
          else 
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../index.php?type=3');
               // echo "password must be more than 6";
               //echo "<script>alert('password must be more than 6');window.location='../addUser.php'</script>";
          }
     }
     else 
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../index.php?type=4');
          // echo "password and retype password not the same";
          //echo "<script>alert('password and retype password not the same');window.location='../addUser.php'</script>";
     }      
}
else 
{
     header('Location: ../index.php');
}

?>