<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Bank Details | Mypetslibrary" />
<title>Edit Bank Details | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'userHeaderAfterLogin.php'; ?>
	<div class="width100 same-padding overflow min-height menu-distance2">
    	<p class="review-product-name left-align-p">Credit Card (2)</p><a href="bankCard.php"><div class="left-add-btn green-button white-text clean2">Add</div></a>
        <div class="clear"></div>
        <form><!-- Delete form-->
        <table class="card-table">
        	<tr>
            	<td>Card No. XXXX XXXX 1200</td>
                <td><a href="bankCardEdit.php" class="green-a hover1"> <img src="img/edit.png" class="edit-png hover1a" alt="Edit" title="Edit"><img src="img/edit2.png" class="edit-png hover1b" alt="Edit" title="Edit">Edit</a></td>
                <td><div class="transparent-button clean delete-btn hover1 open-confirm"><img src="img/close2.png" class="edit-png hover1a" alt="Delete" title="Delete"><img src="img/close3.png" class="edit-png hover1b" alt="Delete" title="Delete">Delete</div></td>
            </tr>
        	<tr>
            	<td>Card No. XXXX XXXX 2200</td>
                <td><a href="bankCardEdit.php" class="green-a hover1"> <img src="img/edit.png" class="edit-png hover1a" alt="Edit" title="Edit"><img src="img/edit2.png" class="edit-png hover1b" alt="Edit" title="Edit">Edit</a></td>
                <td><div class="transparent-button clean delete-btn hover1 open-confirm"><img src="img/close2.png" class="edit-png hover1a" alt="Delete" title="Delete"><img src="img/close3.png" class="edit-png hover1b" alt="Delete" title="Delete">Delete</div></td>
            </tr>            
        </table>
    	<div class="clear"></div>
        	

    	<p class="review-product-name left-align-p">Bank Account (2)</p><a href="bankDetails.php"><div class="left-add-btn green-button white-text clean2">Add</div></a>
        <div class="clear"></div>
        <table class="card-table">
        	<tr>
            	<td>Bank No. XXXX XXXX 1200</td>
                <td><a href="bankDetailsEdit.php" class="green-a hover1"> <img src="img/edit.png" class="edit-png hover1a" alt="Edit" title="Edit"><img src="img/edit2.png" class="edit-png hover1b" alt="Edit" title="Edit">Edit</a></td>
                <td><div class="transparent-button clean delete-btn hover1 open-confirm"><img src="img/close2.png" class="edit-png hover1a" alt="Delete" title="Delete"><img src="img/close3.png" class="edit-png hover1b" alt="Delete" title="Delete">Delete</div></td>
            </tr>
        	<tr>
            	<td>Bank No. XXXX XXXX 2200</td>
                <td><a href="bankDetailsEdit.php" class="green-a hover1"> <img src="img/edit.png" class="edit-png hover1a" alt="Edit" title="Edit"><img src="img/edit2.png" class="edit-png hover1b" alt="Edit" title="Edit">Edit</a></td>
                <td><div class="transparent-button clean delete-btn hover1 open-confirm"><img src="img/close2.png" class="edit-png hover1a" alt="Delete" title="Delete"><img src="img/close3.png" class="edit-png hover1b" alt="Delete" title="Delete">Delete</div></td>
            </tr>            
        </table>
        <!-- Double Confirm Modal -->
        <div id="confirm-modal" class="modal-css">
        
          <!-- Modal content -->
          <div class="modal-content-css confirm-modal-margin">
            <span class="close-css close-confirm">&times;</span>
            <div class="clear"></div>
            <h2 class="green-text h2-title confirm-title">Confirm Delete?</h2>
            <div class="clean cancel-btn close-confirm">Cancel</div>
            <button class="clean red-btn delete-btn2">Delete</button>
            <div class="clear"></div>
            
               
          </div>
        
        </div>        
        </form>
    	<div class="clear"></div>
















	</div>
<div class="clear"></div>


<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>