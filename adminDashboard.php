<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Admin Dashboard | Mypetslibrary" />
<title>Admin Dashboard | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance">
	<h1 class="green-text h1-title">Dashboard</h1>
	<div class="green-border"></div>
    <div class="clear"></div>
    <div class="width100 border-separation">
    	<a href="allUsers.php" class="opacity-hover">
            <div class="white-dropshadow-box four-div-box">
                <img src="img/pet-sellers.png" alt="Total Users" title="Total Users" class="four-div-img">
                <p class="four-div-p">Total Users</p>
                <p class="four-div-amount-p"><b>1000</b></p>
            </div>
        </a>
        <a href="seller.php" class="opacity-hover">
            <div class="white-dropshadow-box four-div-box second-four-div-box left-four-div">
                <img src="img/pet-owner.png" alt="Total Sellers" title="Total Sellers" class="four-div-img">
                <p class="four-div-p">Total Sellers</p>
                <p class="four-div-amount-p"><b>50</b></p>
            </div> 
        </a>
        <a href="petSummary.php" class="opacity-hover">
            <div class="white-dropshadow-box four-div-box right-four-div">
                <img src="img/kitten.png" alt="Total Available Pets" title="Total Available Pets" class="four-div-img">
                <p class="four-div-p">Total Available Pets</p>
                <p class="four-div-amount-p"><b>15000</b></p>
            </div>  
        </a>
        <a class="opacity-hover">       
            <div class="white-dropshadow-box four-div-box second-four-div-box forth-div">
                <img src="img/income.png" alt="Product Sales (RM)" title="Product Sales (RM)" class="four-div-img">
                <p class="four-div-p">Product Sales (RM)</p>
                <p class="four-div-amount-p"><b>3,000,000.00</b></p>
            </div>  
        </a>
        </div>
        <div class="clear"></div>
        <h1 class="green-text h1-title">Pending</h1>
        <div class="green-border"></div>        
        <div class="width100 border-separation">
        <a class="opacity-hover">       
            <div class="white-dropshadow-box four-div-box">
                <img src="img/pending-pet.png" alt="Pending Pets" title="Pending Pets" class="four-div-img">
                <p class="four-div-p">Pending Pets</p>
                <p class="four-div-amount-p"><b>40</b></p>
            </div> 
        </a>
        <a class="opacity-hover">
            <div class="white-dropshadow-box four-div-box second-four-div-box left-four-div">
                <img src="img/review-pet.png" alt="Pending Reviews" title="Pending Reviews" class="four-div-img">
                <p class="four-div-p">Pending Reviews</p>
                <p class="four-div-amount-p"><b>10</b></p>
            </div> 
        </a>
        <a class="opacity-hover">
            <div class="white-dropshadow-box four-div-box right-four-div">
                <img src="img/pet-article.png" alt="Pending Articles" title="Pending Articles" class="four-div-img">
                <p class="four-div-p">Pending Articles</p>
                <p class="four-div-amount-p"><b>10</b></p>
            </div>  
        </a>
        <a class="opacity-hover">       
            <div class="white-dropshadow-box four-div-box second-four-div-box forth-div">
                <img src="img/delivery.png" alt="Shipping Requests" title="Shipping Requests" class="four-div-img">
                <p class="four-div-p">Shipping Requests</p>
                <p class="four-div-amount-p"><b>10</b></p>
            </div>  
        </a>        
        
        
        
                      
    </div>    

</div>
<div class="clear"></div>


<?php include 'js.php'; ?>





</body>
</html>