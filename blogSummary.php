<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Blog Summary | Mypetslibrary" />
<title>Blog Summary | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance">
	<h1 class="green-text h1-title">Blog Summary</h1>
	<div class="green-border"></div>
    <div class="clear"></div>
    <div class="width100 border-separation">
    	<a href="pendingArticle.php" class="opacity-hover">
            <div class="white-dropshadow-box four-div-box">
                <img src="img/review-pet.png" alt="Pending Article" title="Pending Article" class="four-div-img">
                <p class="four-div-p">Pending Article</p>
                <p class="four-div-amount-p"><b>1000</b></p>
            </div>
        </a>
        <a href="approvedArticle.php"  class="opacity-hover">
            <div class="white-dropshadow-box four-div-box second-four-div-box left-four-div">
                <img src="img/approve.png" alt="Approved Article" title="Approved Article" class="four-div-img">
                <p class="four-div-p">Approved Article</p>
                <p class="four-div-amount-p"><b>250</b></p>
            </div> 
        </a>
        <a  href="rejectedArticle.php" class="opacity-hover">
            <div class="white-dropshadow-box four-div-box right-four-div">
                <img src="img/reject.png" alt="Rejected Article" title="Rejected Article" class="four-div-img">
                <p class="four-div-p">Rejected Article</p>
                <p class="four-div-amount-p"><b>100</b></p>
            </div>  
        </a>
        <a href="reportedArticle.php" class="opacity-hover">       
            <div class="white-dropshadow-box four-div-box second-four-div-box forth-div">
                <img src="img/attention.png" alt="Reported Article" title="Reported Article" class="four-div-img">
                <p class="four-div-p">Reported Article</p>
                <p class="four-div-amount-p"><b>&nbsp;</b></p>
            </div>  
        </a>
        <a href="addArticle.php" class="opacity-hover">       
            <div class="white-dropshadow-box four-div-box">
                <img src="img/writing.png" alt="Write New Article" title="Write New Article" class="four-div-img">
                <p class="four-div-p">Write New Article</p>
                <p class="four-div-amount-p"><b>&nbsp;</b></p>
            </div> 
        </a> 
        <a href="articleDraft.php"  class="opacity-hover">
            <div class="white-dropshadow-box four-div-box second-four-div-box left-four-div second-second-div">
                <img src="img/pet-article.png" alt="Draft" title="Draft" class="four-div-img">
                <p class="four-div-p">Draft</p>
                <p class="four-div-amount-p"><b>&nbsp;</b></p>
            </div> 
        </a>                     
    </div>
    <div class="clear"></div>
    <div class="width100 bottom-spacing"></div>

</div>
<div class="clear"></div>



<?php include 'js.php'; ?>
</body>
</html>