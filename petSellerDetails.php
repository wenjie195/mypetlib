<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Pet Seller Name | Mypetslibrary" />
<title>Pet Seller Name | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary - Pet Seller Name, Puppy Seller, Delivery Services, Pet Grooming." />
<meta name="description" content="Mypetslibrary - Pet Seller Name, Puppy Seller, Delivery Services, Pet Grooming." />
<meta name="keywords" content="pet seller name, Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'userHeaderAfterLogin.php'; ?>
<div class="width100 menu-distance3 same-padding min-height2 pet-seller-all-div">
	<div class="big-info-div width100 overflow">
        <div class="left-image-div">
           <div class="item">            
                <div class="clearfix">
                    <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                        <li data-thumb="img/pet-seller.jpg" class="pet-slider-li"> 
                            <img src="img/pet-seller.jpg" class="pet-slider-img"  alt="Pet Seller Name" title="Pet Seller Name"/>
                        </li>
                        <li data-thumb="img/pet-seller2.jpg" class="pet-slider-li"> 
                            <img src="img/pet-seller2.jpg" class="pet-slider-img" alt="Pet Seller Name" title="Pet Seller Name"  />
                        </li>
    
                    </ul>
                </div>
            </div>
            
            
        </div>
        
        <div class="right-content-div2">
            
            <h1 class="green-text pet-name">Fur Fur Fur Fur Fur Fur Fur Fur Fur Fur Fur</h1>
            <div class="left-address-info">
                <p class="green-text breed-p">Street No., Street, City, Postcode, Georgetown, Penang</p>
                <p class="green-text breed-p"><a href="tel:+60383190000" class="contact-icon hover1">010 - 100 1000</a></p>
            </div>
            <div class="right-info-div">
                <a href="tel:+60383190000" class="contact-icon hover1 three-button-width">
                    <img src="img/call.png" class="hover1a" alt="Call" title="Call">
                    <img src="img/call2.png" class="hover1b" alt="Call" title="Call">
                </a>
                <a class="contact-icon hover1 three-button-width">
                    <img src="img/sms.png" class="hover1a" alt="SMS" title="SMS">
                    <img src="img/sms2.png" class="hover1b" alt="SMS" title="SMS">
                </a>  
                <a class="contact-icon hover1 three-button-width">
                    <img src="img/whatsapp.png" class="hover1a" alt="Whatsapp" title="Whatsapp">
                    <img src="img/whatsapp2.png" class="hover1b" alt="Whatsapp" title="Whatsapp">
                </a>  
            </div>
            <div class="clear"></div>
            <a href="petSellerReview.php">
                <div class="review-div hover1">
                    <p class="left-review-p grey-p">Reviews</p>
                    <p class="left-review-mark">4/5</p>
                    <p class="right-review-star">
                        <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                        <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                        <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                        <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                        <img src="img/grey-star.png" alt="Review" title="Review" class="star-img last-star">
                    </p>
                    <p class="right-arrow">
                        <img src="img/arrow.png" alt="Review" title="Review" class="arrow-img hover1a">
                        <img src="img/arrow2.png" alt="Review" title="Review" class="arrow-img hover1b">
                    </p>	
                </div>
            </a>
            <div class="clear"></div>
                <div class="pet-details-div">
                    <table class="pet-table">
                        <tr>
                            <td class="grey-p">Type of Breed</td>
                            <td class="grey-p">:</td>
                            <td>Pomeranian, Pembroke Welsh Corgi, French Bulldog</td>
                        </tr>
                        <tr>
                            <td class="grey-p">Experience</td>
                            <td class="grey-p">:</td>
                            <td>10 Years</td>
                        </tr>  
                        <tr>
                            <td class="grey-p">Joined Date</td>
                            <td class="grey-p">:</td>
                            <td>1/1/2019</td>
                        </tr>
                        <tr>
                            <td class="grey-p">Services</td>
                            <td class="grey-p">:</td>
                            <td>Puppy Seller, Delivery Services, Pet Grooming</td>
                        </tr>  
                        <tr>
                            <td class="grey-p">Other Info</td>
                            <td class="grey-p">:</td>
                            <td>www.facebook.com/xxxxx</td>
                        </tr>
                                                                                                                                            
                    </table>
                    
                </div>  
                       
            </div>
        </div>
        <div class="clear"></div>
        <h1 class="green-text seller-h1">Seller Listings</h1>
	<div class="clear"></div>    


        <div class="width103">
            <a href="puppyDetails.php">
            
                <div class="shadow-white-box featured four-box-size">
                        <!-- Display none or add class hidden if the dog not yet sold -->
        			  <div class="sold-label2">Sold</div>
                      <div class="width100 white-bg">
                        <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                            <p class="slider-product-price">RM5XXX</p>
                            <p class="width100 text-overflow slider-location">Penang</p>
                      </div>
                </div>
            </a> 
            <a href="puppyDetails.php">
                <div class="shadow-white-box featured four-box-size">
                      <div class="width100 white-bg">
                        <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                            <p class="slider-product-price">RM5XXX</p>
                            <p class="width100 text-overflow slider-location">Penang</p>
                      </div>
                </div>
            </a>        
            <a href="puppyDetails.php">
                <div class="shadow-white-box featured four-box-size">
                      <div class="width100 white-bg">
                        <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                            <p class="slider-product-price">RM5XXX</p>
                            <p class="width100 text-overflow slider-location">Penang</p>
                      </div>
                </div>
            </a> 
            <a href="puppyDetails.php">
                <div class="shadow-white-box featured four-box-size">
                      <div class="width100 white-bg">
                        <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                            <p class="slider-product-price">RM5XXX</p>
                            <p class="width100 text-overflow slider-location">Penang</p>
                      </div>
                </div>
            </a>         
            <a href="puppyDetails.php">
                <div class="shadow-white-box four-box-size">
                      <div class="width100 white-bg">
                        <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                            <p class="slider-product-price">RM5XXX</p>
                            <p class="width100 text-overflow slider-location">Penang</p>
                      </div>
                </div>
            </a> 
            <a href="puppyDetails.php">
                <div class="shadow-white-box four-box-size">
                      <div class="width100 white-bg">
                        <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                            <p class="slider-product-price">RM5XXX</p>
                            <p class="width100 text-overflow slider-location">Penang</p>
                      </div>
                </div>
            </a>        
            <a href="puppyDetails.php">
                <div class="shadow-white-box four-box-size">
                      <div class="width100 white-bg">
                        <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                            <p class="slider-product-price">RM5XXX</p>
                            <p class="width100 text-overflow slider-location">Penang</p>
                      </div>
                </div>
            </a> 
            <a href="puppyDetails.php">
                <div class="shadow-white-box four-box-size">
                      <div class="width100 white-bg">
                        <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                            <p class="slider-product-price">RM5XXX</p>
                            <p class="width100 text-overflow slider-location">Penang</p>
                      </div>
                </div>
            </a>                  	

</div>
                       
        
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>
<div class="sticky-distance2 width100">

</div>
<div class="sticky-call-div">
	<a  href="tel:+60383190000" class="three-div-width text-center sticky-call-style clean transparent-button" >
    	Call
    </a>
	<button class="three-div-width text-center sticky-call-style clean transparent-button">
    	SMS
    </button> 
	<button class="three-div-width text-center sticky-call-style clean transparent-button">
    	Whatsapp
    </button>       
</div>

<?php include 'stickyFooter.php'; ?>

</body>
</html>