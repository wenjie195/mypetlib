<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
                <img src="img/mypetslibrary.png" class="logo-img" alt="MyPetsLibrary" title="MyPetsLibrary">
            </div>
            <div class="right-menu-div float-right">
                <div class="dropdown hover1">            
                    <a   class="menu-padding hover1">
                        <img src="img/home1a.png" class="menu-img hover1a" alt="Admin Dashboard" title="Admin Dashboard">
                        <img src="img/home2a.png" class="menu-img hover1b" alt="Admin Dashboard" title="Admin Dashboard">
                    </a>
                	<div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="adminDashboard.php"  class="menu-padding dropdown-a">Dashboard</a></p>
                                <p class="dropdown-p"><a href="homeSettings.php"  class="menu-padding dropdown-a">Homepage Settings</a></p>
                                <p class="dropdown-p"><a href="slider.php"  class="menu-padding dropdown-a">Slider</a></p>
                                <p class="dropdown-p"><a href="featuredProducts.php"  class="menu-padding dropdown-a">Featured Products</a></p>
                                <p class="dropdown-p"><a href="featuredPets.php"  class="menu-padding dropdown-a">Featured Pets</a></p>
                                <p class="dropdown-p"><a href="featuredPartners.php"  class="menu-padding dropdown-a">Featured Partners</a></p>                                
                	</div>                
                </div>
                <div class="dropdown hover1">
                	<a  class="menu-margin menu-padding hover1">
                            	<img src="img/seller.png" class="menu-img hover1a" alt="Seller" title="Seller">
                                <img src="img/seller2.png" class="menu-img hover1b" alt="Seller" title="Seller">
                	</a>
                	<div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="seller.php"  class="menu-padding dropdown-a">All Sellers</a></p>
                                <p class="dropdown-p"><a href="addSeller.php"  class="menu-padding dropdown-a">Add Sellers</a></p>
                	</div>
                </div>  
                <div class="dropdown hover1">
                	<a  class="menu-margin menu-padding hover1">
                            	<img src="img/pet.png" class="menu-img hover1a" alt="Pet" title="Pet">
                                <img src="img/pet2.png" class="menu-img hover1b" alt="Pet" title="Pet">
                	</a>
                	<div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="petSummary.php"  class="menu-padding dropdown-a">Pet Summary</a></p>
                                <p class="dropdown-p"><a href="allPets.php"  class="menu-padding dropdown-a">All Pets</a></p>
                                <p class="dropdown-p"><a href="pendingPets.php"  class="menu-padding dropdown-a">Pending Pets</a></p>
                                <p class="dropdown-p"><a href="pendingPetsPhoto.php"  class="menu-padding dropdown-a">Pending Pet Photos</a></p>
                	</div>
                </div>                 
                <div class="dropdown hover1">
                	<a  class="menu-margin menu-padding hover1">
                            	<img src="img/product.png" class="menu-img hover1a" alt="Product" title="Product">
                                <img src="img/product2.png" class="menu-img hover1b" alt="Product" title="Product">
                	</a>
                	<div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="productSummary.php"  class="menu-padding dropdown-a">Product Summary</a></p>
                                <p class="dropdown-p"><a href="allProducts.php"  class="menu-padding dropdown-a">All Products</a></p>
                                <p class="dropdown-p"><a href="shippingRequest.php"  class="menu-padding dropdown-a">Shipping</a></p>
                                <p class="dropdown-p"><a href="allSales.php"  class="menu-padding dropdown-a">Sales</a></p>
                                <p class="dropdown-p"><a href="addProduct.php"  class="menu-padding dropdown-a">Add New Product</a></p>
                                <p class="dropdown-p"><a href="category.php"  class="menu-padding dropdown-a">Category</a></p>
                                <p class="dropdown-p"><a href="brand.php"  class="menu-padding dropdown-a">Brand</a></p>
                	</div>
                </div>                
                <div class="dropdown hover1">
                	<a  class="menu-margin menu-padding hover1">
                            	<img src="img/review.png" class="menu-img hover1a" alt="Review" title="Review">
                                <img src="img/review2.png" class="menu-img hover1b" alt="Review" title="Review">
                	</a>
                	<div class="dropdown-content yellow-dropdown-content">
                    			<p class="dropdown-p"><a href="reviewSummary.php"  class="menu-padding dropdown-a">Review Summary</a></p>
                                <p class="dropdown-p"><a href="pendingReview.php"  class="menu-padding dropdown-a">Pending Reviews</a></p>
                                <p class="dropdown-p"><a href="approvedReview.php"  class="menu-padding dropdown-a">Approved Reviews</a></p>
                                <p class="dropdown-p"><a href="rejectedReview.php"  class="menu-padding dropdown-a">Rejected Reviews</a></p>
                                <p class="dropdown-p"><a href="reportedReview.php"  class="menu-padding dropdown-a">Reported Reviews</a></p>
                	</div>
                </div>                  
                <div class="dropdown hover1">
                	<a  class="menu-margin menu-padding hover1">
                            	<img src="img/article.png" class="menu-img hover1a" alt="Article" title="Article">
                                <img src="img/article2.png" class="menu-img hover1b" alt="Article" title="Article">
                	</a>
                	<div class="dropdown-content yellow-dropdown-content">
                    			<p class="dropdown-p"><a href="blogSummary.php"  class="menu-padding dropdown-a">Blog Summary</a></p>
                                <p class="dropdown-p"><a href="pendingArticle.php"  class="menu-padding dropdown-a">Pending Article</a></p>
                                <p class="dropdown-p"><a href="approvedArticle.php"  class="menu-padding dropdown-a">Approved Article</a></p>
                                <p class="dropdown-p"><a href="rejectedArticle.php"  class="menu-padding dropdown-a">Rejected Article</a></p>
                                <p class="dropdown-p"><a href="reportedArticle.php"  class="menu-padding dropdown-a">Reported Article</a></p>
                                <p class="dropdown-p"><a href="addArticle.php"  class="menu-padding dropdown-a">Write New Article</a></p>
                                <p class="dropdown-p"><a href="articleDraft.php"  class="menu-padding dropdown-a">Draft</a></p>                                
                	</div>
                </div>    
                <div class="dropdown hover1">
                	<a  class="menu-margin menu-padding hover1">
                            	<img src="img/user.png" class="menu-img hover1a" alt="User" title="User">
                                <img src="img/user2.png" class="menu-img hover1b" alt="User" title="User">
                	</a>
                	<div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="allUsers.php"  class="menu-padding dropdown-a">All Users</a></p>
                                <p class="dropdown-p"><a href=""  class="menu-padding dropdown-a">Banned Users</a></p>
                                <p class="dropdown-p"><a href=""  class="menu-padding dropdown-a">Add New User</a></p>                               
                	</div>
                </div>                 
                <div class="dropdown hover1">
                	<a  class="menu-margin menu-padding hover1">
                            	<img src="img/settings.png" class="menu-img hover1a" alt="Settings" title="Settings">
                                <img src="img/settings2.png" class="menu-img hover1b" alt="Settings" title="Settings">
                	</a>
                	<div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href=""  class="menu-padding dropdown-a">Change Email</a></p>
                                <p class="dropdown-p"><a href=""  class="menu-padding dropdown-a">Change Password</a></p>
                                <p class="dropdown-p"><a href=""  class="menu-padding dropdown-a">Logout</a></p>                               
                	</div>
                </div>                  
					
                    
                    <div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">Open Menu</button>
						<ul class="dl-menu">
								  <li><a href="adminDashboard.php">Dashboard</a></li>
                                  <li><a href="homeSettings.php">Homepage Settings</a></li>
                                  <li><a href="seller.php" class="mini-li">All Sellers</a></li>
                                  <li><a href="petSummary.php" class="mini-li">Pet Summary</a></li>
                                  <li><a href="allPets.php" class="mini-li">All Pets</a></li>
                                  <li><a href="productSummary.php" class="mini-li">Product Summary</a>                                                   
                                  <li><a href="reviewSummary.php"  class="mini-li">Review Summary</a></li>   
                                  <li><a href="blogSummary" class="mini-li">Blog Summary</a></li>
                                  <li><a href=""  class="mini-li">All Users</a></li>                                   
                                  <li><a href="" class="mini-li">Change Email</a></li>
                                  <li><a href=""  class="mini-li">Change Password</a></li>                                                                      
                                  <li  class="last-li"><a href="logout.php">Logout</a></li>
						</ul>
					</div><!-- /dl-menuwrapper -->                
                
                                       	
            </div>
        </div>

</header>
