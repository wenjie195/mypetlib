<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Shipping Address | Mypetslibrary" />
<title>Edit Shipping Address | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'userHeaderAfterLogin.php'; ?>
	<div class="width100 same-padding overflow min-height menu-distance2">
    	<p class="review-product-name">Shipping Address</p>
 		<form>
        <div class="dual-input">
        	<p class="input-top-p">Receiver Name</p>
        	<input class="input-name clean input-textarea" type="text" placeholder="Receiver Name" required name="" value="">
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p">Receiver Contact No.</p>
        	<input class="input-name clean input-textarea" type="text" placeholder="Receiver Contact Number" required name="" value="">      
        </div>        
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p">State</p>
        	<select class="input-name clean" required >
            	<option>State</option>
                <option>Penang</option>
            </select>      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p">Area</p>
        	<select class="input-name clean" required >
            	<option>Area</option>
                <option>Bayan Baru</option>
            </select>       
        </div>         
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p">Postal Code</p>
        	<input class="input-name clean input-textarea" type="text" placeholder="Postal Code" required name="" value>     
        </div> 
        <div class="clear"></div>
        <div class="width100 overflow">
        	<p class="input-top-p">Address</p>
        	<textarea class="input-name clean address-textarea" type="text" placeholder="Address" required name=""></textarea>      
        </div>         
        <div class="clear"></div>  
        <div class="width100 overflow text-center">     
        	<button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button>
        </div>
        </form>
	</div>
<div class="clear"></div>
<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>