<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Blog | Mypetslibrary" />
<title>Blog | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">

<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
 
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<?php include 'userHeader.php'; ?>
 

<div class="width100 same-padding overflow min-height menu-distance2">
    <h1 class="green-text user-title left-align-title">Blog</h1>
	<div class="clear"></div>
    <a href="pet-blog.php" class="opacity-hover">
        <div class="two-div-width shadow-white-box blog-box opacity-hover">
            <div class="left-img-div2">
            	<img src="img/blog-picture.jpg" class="width100" alt="Blog Title" title="Blog Title">
            </div>
            <div class="right-content-div3">
                <h3 class="article-title text-overflow">
                    Think Thrice Before Getting a Pet
                </h3>
                <p class="date-p">
                    29/11/2019
                </p>
                <p class="right-content-p">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.
                </p>
            </div>
        </div>
    </a>
    <a href="pet-blog.php" class="opacity-hover">
        <div class="two-div-width shadow-white-box second-two-div-width blog-box opacity-hover">
            <div class="left-img-div2">
            	<img src="img/blog-picture.jpg" class="width100" alt="Blog Title" title="Blog Title">
            </div>
            <div class="right-content-div3">
                <h3 class="article-title text-overflow">
                    The Standing Trick Tutorial
                </h3>
                <p class="date-p">
                    29/11/2019
                </p>
                <p class="right-content-p">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.
                </p>
            </div>
        </div>
	</a>
    <div class="clear"></div>
    <a href="pet-blog.php" class="opacity-hover">
        <div class="two-div-width shadow-white-box blog-box opacity-hover">
            <div class="left-img-div2">
            	<img src="img/blog-picture.jpg" class="width100" alt="Blog Title" title="Blog Title">
            </div>
            <div class="right-content-div3">
                <h3 class="article-title text-overflow">
                    Think Thrice Before Getting a Pet
                </h3>
                <p class="date-p">
                    29/11/2019
                </p>
                <p class="right-content-p">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.
                </p>
            </div>
        </div>
    </a>
    <a href="pet-blog.php" class="opacity-hover">
        <div class="two-div-width shadow-white-box second-two-div-width blog-box opacity-hover">
            <div class="left-img-div2">
            	<img src="img/blog-picture.jpg" class="width100" alt="Blog Title" title="Blog Title">
            </div>
            <div class="right-content-div3">
                <h3 class="article-title text-overflow">
                    The Standing Trick Tutorial
                </h3>
                <p class="date-p">
                    29/11/2019
                </p>
                <p class="right-content-p">
                   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.
                </p>
            </div>
        </div>
	</a>    
</div>


  <style type="text/css">

	.blog-a .hover1a{
		display:none !important;}
	.blog-a .hover1b{
		display:inline-block !important;}						
  </style>
<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>