<header id="header" class="header header--fixed same-padding header1 menu-color logged-menu" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
                <a href="index.php"><img src="img/mypetslibrary.png" class="logo-img opacity-hover" alt="MyPetsLibrary" title="MyPetsLibrary"></a>
            </div>
            <div class="right-menu-div float-right text-menu-right">
               <a href="favourite.php" class="menu-padding text-menu-a hover1">
					<img src="img/favourite.png" class="menu-icon hover1a" alt="Favourite" title="Favourite">
                    <img src="img/favourite2.png" class="menu-icon hover1b" alt="Favourite" title="Favourite">
                </a>            
               	<a href="cart.php" class="menu-padding menu-margin text-menu-a hover1">
					<img src="img/cart.png" class="menu-icon hover1a" alt="Cart" title="Cart">
                    <img src="img/cart2.png" class="menu-icon hover1b" alt="Cart" title="Cart">
                </a> 			
               	<a href="index.php" class="menu-padding black-to-white menu-margin text-menu-a">
					Home
                </a>   
               	<a href="profile.php" class="menu-padding black-to-white menu-margin text-menu-a">
					Profile
                </a> 
               	<a href="malaysia-cute-puppy-dog.php" class="menu-padding black-to-white menu-margin text-menu-a">
					Puppies
                </a>   
               	<a href="malaysia-cute-kitten-cat.php" class="menu-padding black-to-white menu-margin text-menu-a">
					Kittens
                </a>                 
               	<a href="malaysia-cute-reptiles.php" class="menu-padding black-to-white menu-margin text-menu-a">
					Reptiles
                </a>   
               	<a href="malaysia-pet-food-toy-product.php" class="menu-padding black-to-white menu-margin text-menu-a">
					Products
                </a>                
                 
               	<a href="malaysia-pet-blog.php" class="menu-padding black-to-white menu-margin text-menu-a">
					Blog
                </a>
                <a href="logout.php"  class="menu-padding black-to-white menu-margin text-menu-a">
					Logout
                </a>                                                                  
                    <div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">Open Menu</button>
						<ul class="dl-menu">
                        	<div class="overflow three-menu-container">
                            	<div class="menu-three-div">
                            		<a href="cart.php" class="opacity-hover">
                                    	<img src="img/cart2.png" alt="Cart" title="Cart" class="menu-three-img">
                                        <div class="red-dot"><p class="red-dot-p">10</p></div>
                                    </a>
                                </div>
                            	<div class="menu-three-div mid-menu-three-div">
                            		<a href="toShip.php" class="opacity-hover">
                                    	<img src="img/shipping2.png" alt="To Ship" title="To Ship" class="menu-three-img">
                                    	<div class="red-dot"><p class="red-dot-p">10</p></div>
                                    </a>
                                </div>  
                            	<div class="menu-three-div">
                            		<a href="toReceive.php" class="opacity-hover">
                                    	<img src="img/receive2.png" alt="To Receive" title="To Receive" class="menu-three-img">
                                    	<div class="red-dot"><p class="red-dot-p">10</p></div>
                                    </a>
                                </div>                                
                                                              
                            </div>
                        		  <li><a href="profile.php" class="mini-li">Profile</a></li>
                                  <li><a href="index.php" class="mini-li">Home</a></li>
                                  <li><a href="malaysia-cute-puppy-dog.php" class="mini-li">Puppies</a></li>                                  
                                  <li><a href="malaysia-cute-kitten-cat.php" class="mini-li">Kittens</a></li>
                                  <li><a href="malaysia-cute-reptiles.php"  class="mini-li">Reptiles</a></li>                                 
                                  <li><a href="malaysia-pet-food-toy-product.php"  class="mini-li">Products</a></li>   
                                  <li><a href="malaysia-pet-blog.php" class="mini-li">Blog</a></li>
                                  <li><a href="logout.php" class="mini-li">Logout</a></li>
						</ul>
					</div><!-- /dl-menuwrapper -->                
                
                                       	
            </div>
        </div>

</header>
