<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add Featured Pets | Mypetslibrary" />
<title>Add Featured Pets | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance  padding-bottom30">
	<div class="width100">
		<div class="left-h1-div featured-left">
            <h1 class="green-text h1-title">Add Featured Pets</h1>
            <div class="green-border"></div>
		</div>        
        
    </div>
    <div class="clear"></div>
    <div class="width100">
    	   <form>
            <input class="line-input clean" type="text" placeholder="Search Pet SKU">
                <button class="search-btn hover1 clean">
                        <img src="img/search.png" class="visible-img hover1a" alt="Search" title="Search">
                        <img src="img/search2.png" class="visible-img hover1b" alt="Search" title="Search">
                </button>
            </form>
    </div>
	<div class="width103 border-separation">

     
            <div class="four-box-size"> 	
                <div class="shadow-white-box">
                      <div class="width100 white-bg">
                        <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                            <p class="width100 text-overflow slider-product-price">JAN-15-PP</p>
                      </div>
                </div>
				<!-- Cannot Add when the limit is reach -->
                <button class="clean green-button featured-same-button">Add</button>
                 <!-- Change to Remove if the pet is already added -->  
                 <!--<button class="clean red-btn featured-same-button">Remove</button>  -->                  
            </div>
   
   
            <div class="four-box-size"> 	
                <div class="shadow-white-box">
                      <div class="width100 white-bg">
                        <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                            <p class="width100 text-overflow slider-product-price">JAN-15-PP</p>
                      </div>
                </div>

                <button class="clean green-button featured-same-button">Add</button>
                                       
            </div>
   

            <div class="four-box-size"> 	
                <div class="shadow-white-box">
                      <div class="width100 white-bg">
                        <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                            <p class="width100 text-overflow slider-product-price">JAN-15-PP</p>
                      </div>
                </div>

                <button class="clean green-button featured-same-button">Add</button>
                                       
            </div>

            <div class="four-box-size"> 	
                <div class="shadow-white-box">
                      <div class="width100 white-bg">
                        <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                            <p class="width100 text-overflow slider-product-price">JAN-15-PP</p>
                      </div>
                </div>

                <button class="clean green-button featured-same-button">Add</button>
                                       
            </div>
  
     
            <div class="four-box-size"> 	
                <div class="shadow-white-box">
                      <div class="width100 white-bg">
                        <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                            <p class="width100 text-overflow slider-product-price">JAN-15-PP</p>
                      </div>
                </div>

                <button class="clean green-button featured-same-button">Add</button>
                                       
            </div>

            <div class="four-box-size"> 	
                <div class="shadow-white-box">
                      <div class="width100 white-bg">
                        <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                            <p class="width100 text-overflow slider-product-price">JAN-15-PP</p>
                      </div>
                </div>

                <button class="clean green-button featured-same-button">Add</button>
                                       
            </div>
   

            <div class="four-box-size"> 	
                <div class="shadow-white-box">
                      <div class="width100 white-bg">
                        <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                            <p class="width100 text-overflow slider-product-price">JAN-15-PP</p>
                      </div>
                </div>

                <button class="clean green-button featured-same-button">Add</button>
                                       
            </div>
 
       
            <div class="four-box-size"> 	
                <div class="shadow-white-box">
                      <div class="width100 white-bg">
                        <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                            <p class="width100 text-overflow slider-product-price">JAN-15-PP</p>
                      </div>
                </div>

                <button class="clean green-button featured-same-button">Add</button>
                                       
            </div>
        
               
        </div>

		<div class="clear"></div>
        <div class="width100 overflow text-center">
        	<button class="green-button white-text clean2 edit-1-btn">Save</button>
        </div>
        </div>        
        <div class="clear"></div>
        




<?php include 'js.php'; ?>
</body>
</html>