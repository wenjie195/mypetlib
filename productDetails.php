<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Pedigree Dentastix Dog Treats | Mypetslibrary" />
<title>Pedigree Dentastix Dog Treats | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary - Pedigree Dentastix Dog Treats - Daily oral care treat for your pet.
Unique tasty X-shaped treat clinically proven to reduce plaque. It has active ingredients zinc sulphate &amp; sodium trio polyphosphate that help in slowly down the rate of tartar build up." />
<meta name="description" content="Mypetslibrary - Pedigree Dentastix Dog Treats - Daily oral care treat for your pet. Unique tasty X-shaped treat clinically proven to reduce plaque. It has active ingredients zinc sulphate &amp; sodium trio polyphosphate that help in slowly down the rate of tartar build up." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'userHeaderAfterLogin.php'; ?>
<div class="width100 menu-distance3 same-padding min-height2 product-details-all-div">
	<div class="left-image-div">
       <div class="item">            
            <div class="clearfix">
                <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                    <li data-thumb="img/product-img.jpg" class="pet-slider-li"> 
                        <img src="img/product-img.jpg" class="pet-slider-img" alt="Product Name" title="Product Name" />
                    </li>
                    <li data-thumb="img/product-1.jpg" class="pet-slider-li"> 
                        <img src="img/product-1.jpg" class="pet-slider-img" alt="Product Name"  title="Product Name"  />
                    </li>
                    <li data-thumb="img/product-2.jpg" class="pet-slider-li"> 
                        <img src="img/product-2.jpg" class="pet-slider-img" alt="Product Name"  title="Product Name"  />
                    </li>
                    <li data-thumb="img/product-3.jpg" class="pet-slider-li"> 
                        <img src="img/product-3.jpg" class="pet-slider-img" alt="Product Name"  title="Product Name"  />
                         </li>
                    <li data-thumb="img/product-4.jpg" class="pet-slider-li"> 
                         <img src="img/product-4.jpg" class="pet-slider-img" alt="Product Name"  title="Product Name"  />
                    </li>
                </ul>
            </div>
        </div>
        <!-- Display none or add class hidden if the dog not yet sold -->
        <div class="sold-label">Sold</div>
        
        
    </div>
    
    <div class="right-content-div2">
    	<p class="green-text breed-p">Pedigree</p>
        <h1 class="green-text pet-name">Pedigree Dentastix Dog Treats</h1>
        <p class="price-p2">RM28.00</p>
        <div class="right-info-div">
        	<div class="contain1000 three-button-width">
                 	<p class="right-sold product-details-p">1000<br>Sold</p>	
            
            </div>
         	<a class="contact-icon hover1 three-button-width">
            	<img src="img/favourite-1.png" class="hover1a" alt="Favourite" title="Favourite">
                <img src="img/favourite-2.png" class="hover1b" alt="Favourite" title="Favourite">
            </a>  

         	<a class="contact-icon hover1 last-contact-icon open-social three-button-width">
                    <img src="img/share.png" class="hover1a" alt="Share" title="Share">
                    <img src="img/share2.png" class="hover1b" alt="Share" title="Share">
            </a>
            
        </div>
        <div class="clear"></div>




		<div class="pet-details-div">
            <div class="tab">
              <button class="tablinks active" onclick="openTab(event, 'Details')">Details</button>
  			  <button class="tablinks" onclick="openTab(event, 'Terms')">Terms</button>
			</div>     
        <div  id="Details" class="tabcontent block">
				<table class="pet-table">
                	<tr>
                    	<td class="grey-p">Stock</td>
                        <td class="grey-p">:</td>
						<td>500</td>
                    </tr>
                	<tr>
                    	<td class="grey-p">Expiry Date</td>
                        <td class="grey-p">:</td>
						<td>Between 1 year</td>
                    </tr>  
                	<tr>
                    	<td class="grey-p">For Animal Type</td>
                        <td class="grey-p">:</td>
						<td>Puppy</td>
                    </tr>
                	<tr>
                    	<td class="grey-p">Category</td>
                        <td class="grey-p">:</td>
						<td>Food</td>
                    </tr>                                                                                                                
                </table>
                <p class="pet-table-p">
                	- Daily oral care treat for your pet.<br>
					- Unique tasty X-shaped treat clinically proven to reduce plaque.<br>
					- It has active ingredients zinc sulphate &amp; sodium trio polyphosphate that help in slow down the rate of tartar build up.
                </p>
        </div>
        
        <div id="Terms" class="tabcontent">
			<p class="pet-table-p">Terms content</p>
        </div>
            <a href="malaysia-pet-food-toy-product.php">
                <div class="review-div hover1 upper-review-div">
                    <p class="left-review-p grey-p">Brand</p>
                    <p class="left-review-mark brand-p text-overflow">Pedigree</p>

                    <p class="right-arrow">
                        <img src="img/arrow.png" alt="Review" title="Review" class="arrow-img hover1a">
                        <img src="img/arrow2.png" alt="Review" title="Review" class="arrow-img hover1b">
                    </p>
                    <p class="beside-right-arrow grey-to-lightgreen">
                        View All Product
                    </p>                   	
                </div>
            </a>        
            <a href="productReview.php">
                <div class="review-div hover1 lower-review-div">
                    <p class="left-review-p grey-p">Reviews</p>
                    <p class="left-review-mark">4/5</p>
                    <p class="right-review-star">
                        <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                        <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                        <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                        <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                        <img src="img/grey-star.png" alt="Review" title="Review" class="star-img last-star">
                    </p>
                    <p class="right-arrow">
                        <img src="img/arrow.png" alt="Review" title="Review" class="arrow-img hover1a">
                        <img src="img/arrow2.png" alt="Review" title="Review" class="arrow-img hover1b">
                    </p>	
                </div>
            </a>


            </div>  
                      
        </div>
        <div class="clear"></div>
    <div class="width100 top-divider">
        <h1 class="green-text user-title left-align-title">Products</h1>
        <a class="right-align-link view-a light-green-a hover-a" href="malaysia-pet-food-toy-product.php">View More</a>
    </div>
    <div class="clear"></div>       
        
	<div class="width103 product-big-div">
        <a href="productDetails.php">
            <div class="shadow-white-box featured four-box-size">
                  <div class="width100 white-bg">
                    <img src="img/product-img.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Pedigree Dentastix Dog Treats</p>
                        <p class="product-price-width text-overflow slider-product-price left-price">RM2800.00</p>
                        <p class="slider-product-price right-like hover1">
                        	<img src="img/favourite-1.png" class="hover1a like-img width100" alt="Favourite" title="Favourite">
                            <img src="img/favourite-2.png" class="hover1b like-img width100" alt="Favourite" title="Favourite">
                        </p>
                        <div class="clear"></div>
                        <p class="left-rating">5<img src="img/yellow-star.png" class="rating-tiny-star" alt="Rating" title="Rating"></p>
                        <p class="right-sold">1000 <span class="sold-color">Sold</span></p>
                  </div>
            </div>
        </a> 
        <a href="productDetails.php">
            <div class="shadow-white-box featured four-box-size">
                  <div class="width100 white-bg">
                    <img src="img/product-img.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Pedigree Dentastix Dog Treats</p>
                        <p class="product-price-width text-overflow slider-product-price left-price">RM2800.00</p>
                        <p class="slider-product-price right-like hover1">
                        	<img src="img/favourite-1.png" class="hover1a like-img width100" alt="Favourite" title="Favourite">
                            <img src="img/favourite-2.png" class="hover1b like-img width100" alt="Favourite" title="Favourite">
                        </p>
                        <div class="clear"></div>
                        <p class="left-rating">5<img src="img/yellow-star.png" class="rating-tiny-star" alt="Rating" title="Rating"></p>
                        <p class="right-sold">1000 <span class="sold-color">Sold</span></p>
                  </div>
            </div>
        </a>        
        <a href="productDetails.php">
            <div class="shadow-white-box featured four-box-size">
                  <div class="width100 white-bg">
                    <img src="img/product-img.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Pedigree Dentastix Dog Treats</p>
                        <p class="product-price-width text-overflow slider-product-price left-price">RM2800.00</p>
                        <p class="slider-product-price right-like hover1">
                        	<img src="img/favourite-1.png" class="hover1a like-img width100" alt="Favourite" title="Favourite">
                            <img src="img/favourite-2.png" class="hover1b like-img width100" alt="Favourite" title="Favourite">
                        </p>
                        <div class="clear"></div>
                        <p class="left-rating">5<img src="img/yellow-star.png" class="rating-tiny-star" alt="Rating" title="Rating"></p>
                        <p class="right-sold">1000 <span class="sold-color">Sold</span></p>
                  </div>
            </div>
        </a> 
        <a href="productDetails.php">
            <div class="shadow-white-box featured four-box-size">
                  <div class="width100 white-bg">
                    <img src="img/product-img.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Pedigree Dentastix Dog Treats</p>
                        <p class="product-price-width text-overflow slider-product-price left-price">RM2800.00</p>
                        <p class="slider-product-price right-like hover1">
                        	<img src="img/favourite-1.png" class="hover1a like-img width100" alt="Favourite" title="Favourite">
                            <img src="img/favourite-2.png" class="hover1b like-img width100" alt="Favourite" title="Favourite">
                        </p>
                        <div class="clear"></div>
                        <p class="left-rating">5<img src="img/yellow-star.png" class="rating-tiny-star" alt="Rating" title="Rating"></p>
                        <p class="right-sold">1000 <span class="sold-color">Sold</span></p>
                  </div>
            </div>
        </a>         		
	</div>        
</div>
<div class="clear"></div>
<div class="sticky-call-div open-variation red-btn block">
	Amount and Variation
</div>
<?php include 'js.php'; ?>
<div class="sticky-distance3 width100">

</div>

<?php include 'stickyFooter.php'; ?>
</body>
</html>