<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Change Phone Number | Mypetslibrary" />
<title>Change Phone Number | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'userHeaderAfterLogin.php'; ?>
	<div class="width100 same-padding overflow min-height menu-distance2">
    	<p class="review-product-name">Change Phone Number</p>
 		<form>
        <div class="dual-input">
        	<p class="input-top-p">New Phone No.</p>
        	<input class="input-name clean" type="text" placeholder="New Phone Number" required name="">   
            <div class="clear"></div>
            <button class="orange-button white-text width100 clean2 bottom-distance20 width100 ow-no-margin-top">Request TAC</button>
            <div class="clear"></div>
            <p class="input-top-p">TAC</p>
            <input class="input-name clean ow-no-margin-top" type="text" placeholder="TAC" required name="">   
        </div>
        <div class="clear"></div>
        	<button class="green-button white-text clean2 edit-1-btn">Submit</button>
        </form>
	</div>
<div class="clear"></div>
<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>