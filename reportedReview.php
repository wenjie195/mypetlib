<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Reported Review | Mypetslibrary" />
<title>Reported Review | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance">
	<div class="width100">
        <div class="width100 ship-top-div">
            <h1 class="green-text h1-title"><a href="pendingReview.php" class="green-a">Pending Reviews</a> | <a href="approvedReview.php" class="green-a">Approved</a> | <a href="rejectedReview.php" class="green-a">Rejected</a> | Reported</h1>
            <div class="green-border"></div>
        </div>
        <div class="width100 ship-bottom-div">
                <form>
                <input class="line-input clean" type="text" placeholder="Search">
                    <button class="search-btn hover1 clean">
                            <img src="img/search.png" class="visible-img hover1a" alt="Search" title="Search">
                            <img src="img/search2.png" class="visible-img hover1b" alt="Search" title="Search">
                    </button>
                </form>

        </div>  
    </div>


    <div class="clear"></div>
	<div class="width100 scroll-div small-spacing">
    	<table class="order-table ow-width100">
        	<thead>
            	<tr>
                	<th class="first-column table-green-a">No.</th>
                    <th>For</th>
                    <th>Review</th>
                    <th>Reported by</th>
                    <th>Action</th>                 
                </tr>
            </thead>
            <tbody>
            	<tr>
                	<td class="first-column">1.</td>
                    <td><a href="productDetails.php" class="green-a table-green-a">Pedigree Dentastix Puppy 56g Dog Treats</a></td>
                    <td>
                    <div class="table-left-review-profile">
                        <img src="img/pet-seller2.jpg" class="profile-pic-css">
                    </div>                    
                    <div class="table-left-review-data">
                        <p class="table-review-username-p">Jack Lim</p>
                        <div class="review-star-div">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/grey-star.png" alt="Review" title="Review" class="table-star-img">
                        </div>
                    </div>               
                    <div class="clear"></div>     
                    <div class="table-review-comment">
                           <a href="./img/product-1.jpg" data-fancybox="images-preview"  >
                                <img src="img/product-1.jpg" class="table-review-img opacity-hover" >
                           </a>
                       	   <p class="table-review-p">High quality product with a reasonable price!</p>
                           <p class="table-date-p">12/12/2019</p>
                    </div>                     
                    </td>
                    <td>
                    <div class="table-left-review-profile">
                        <img src="img/profile-pic.jpg" class="profile-pic-css">
                    </div>                    
                    <div class="table-left-review-data">
                        <p class="table-review-username-p">User 1</p>
                    </div>               
                    <div class="clear"></div>     
                    <div class="table-review-comment">
                       	   <p class="table-review-p">Spam good review.</p>
                           <p class="table-date-p">12/12/2019</p>
                    </div>                     
                    </td>                    
                    <td>
                    <button class="approve-button right-margin-10px opacity-hover clean transparent-button"><img src="img/approve.png" class="approve-png" alt="Keep the Review" title="Keep the Review"></button>
                    <button class="approve-button opacity-hover clean transparent-button"><img src="img/reject.png" class="approve-png" alt="Remove Review" title="Remove Review"></button>
                    
                    </td>
                    
                </tr>
            	<tr>
                	<td class="first-column table-green-a">2.</td>
                    <td><a href="productDetails.php" class="green-a table-green-a">Pedigree Dentastix Puppy 56g Dog Treats</a></td>
                    <td>
                        <div class="table-left-review-profile">
                            <img src="img/pet-seller2.jpg" class="profile-pic-css">
                        </div>                    
                        <div class="table-left-review-data">
                            <p class="table-review-username-p">Jack Lim</p>
                            <div class="review-star-div">
                                <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                                <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                                <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                                <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                                <img src="img/grey-star.png" alt="Review" title="Review" class="table-star-img">
                            </div>
                        </div>               
                        <div class="clear"></div>     
                        <div class="table-review-comment">
                               <a href="./img/product-1.jpg" data-fancybox="images-preview"  >
                                    <img src="img/product-1.jpg" class="table-review-img opacity-hover" >
                               </a>
                               <p class="table-review-p">High quality product with a reasonable price!</p>
                               <p class="table-date-p">12/12/2019</p>
                        </div>                     
                    </td>
                    <td>
                        <div class="table-left-review-profile">
                            <img src="img/profile-pic.jpg" class="profile-pic-css">
                        </div>                    
                        <div class="table-left-review-data">
                            <p class="table-review-username-p">User 1</p>
                        </div>               
                        <div class="clear"></div>     
                        <div class="table-review-comment">
                               <p class="table-review-p">Spam good review.</p>
                               <p class="table-date-p">12/12/2019</p>
                        </div>                     
                    </td>                     
                    <td>
                    <button class="approve-button right-margin-10px opacity-hover clean transparent-button"><img src="img/approve.png" class="approve-png" alt="Keep the Review" title="Keep the Review"></button>
                    <button class="approve-button opacity-hover clean transparent-button"><img src="img/reject.png" class="approve-png" alt="Remove Review" title="Remove Review"></button>
                    
                    </td>
                    
                </tr>
            	<tr>
                	<td class="first-column table-green-a">3.</td>
                    <td><a href="productDetails.php" class="green-a table-green-a">Pedigree Dentastix Puppy 56g Dog Treats</a></td>
                    <td>
                    <div class="table-left-review-profile">
                        <img src="img/pet-seller2.jpg" class="profile-pic-css">
                    </div>                    
                    <div class="table-left-review-data">
                        <p class="table-review-username-p">Jack Lim</p>
                        <div class="review-star-div">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/yellow-star.png" alt="Review" title="Review" class="table-star-img">
                            <img src="img/grey-star.png" alt="Review" title="Review" class="table-star-img">
                        </div>
                    </div>               
                    <div class="clear"></div>     
                    <div class="table-review-comment">
                           <a href="./img/product-1.jpg" data-fancybox="images-preview"  >
                                <img src="img/product-1.jpg" class="table-review-img opacity-hover" >
                           </a>
                       	   <p class="table-review-p">High quality product with a reasonable price!</p>
                           <p class="table-date-p">12/12/2019</p>
                    </div>                     
                    </td>
                    <td>
                        <div class="table-left-review-profile">
                            <img src="img/profile-pic.jpg" class="profile-pic-css">
                        </div>                    
                        <div class="table-left-review-data">
                            <p class="table-review-username-p">User 1</p>
                        </div>               
                        <div class="clear"></div>     
                        <div class="table-review-comment">
                               <p class="table-review-p">Spam good review.</p>
                               <p class="table-date-p">12/12/2019</p>
                        </div>                     
                    </td>                    
                    <td>
                    <button class="approve-button right-margin-10px opacity-hover clean transparent-button"><img src="img/approve.png" class="approve-png" alt="Keep the Review" title="Keep the Review"></button>
                    <button class="approve-button opacity-hover clean transparent-button"><img src="img/reject.png" class="approve-png" alt="Remove Review" title="Remove Review"></button>
                    
                    </td>
                    
                </tr>                              
            </tbody>
        </table>
    </div>
    <div class="clear"></div>
    <div class="width100 bottom-spacing"></div>

</div>
<div class="clear"></div>



<?php include 'js.php'; ?>
</body>
</html>