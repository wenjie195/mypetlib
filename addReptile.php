<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add a Reptile | Mypetslibrary" />
<title>Add a Reptile | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'userHeaderAfterLogin.php'; ?>
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
            <h1 class="green-text h1-title">Add a Reptile</h1>
            <div class="green-border"></div>
   </div>
   <div class="border-separation">
        <div class="clear"></div>
 		<form>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Pet Name*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Pet Name" required name="">      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">SKU*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="SKU" required name="">     
        </div>        
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Pet Slug (for URL, no spacing or contain any symbol except -)</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Pet Slug"  required name="">      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Pet Price (RM)*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Pet Price (RM)"  required name="">    
        </div> 
        <div class="clear"></div>  
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Pet Age*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Pet Age" name=""  required>      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Vaccinated Status*</p>
        	<select class="input-name clean admin-input" required >
            	<option>Yes</option>
                <option>No</option>
            </select>   
        </div>           
		<div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Dewormed Status*</p>
        	<select class="input-name clean admin-input" required >
            	<option>Yes</option>
                <option>No</option>
            </select>       
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Gender*</p>
        	<select class="input-name clean admin-input" required >
            	<option>Male</option>
                <option>Female</option>
            </select>   
        </div>  
        
        <div class="clear"></div>
         <div class="dual-input">
        	<p class="input-top-p admin-top-p">Pet Color*</p>
        	<select class="input-name clean admin-input" required >
            	<option>White</option>
                <option>Black</option>
            </select>       
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Pet Size</p>
        	<select class="input-name clean admin-input" required >
            	<option>Small</option>
                <option>Middle</option>
                <option>Big</option>
            </select>   
        </div>  
        <div class="clear"></div>
         <div class="dual-input">
        	<p class="input-top-p admin-top-p">Status*</p>
        	<select class="input-name clean admin-input" required >
            	<option>Available</option>
                <option>Sold</option>
            </select>       
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Feature</p>
        	<select class="input-name clean admin-input" required >
            	<option>Yes</option>
                <option>No</option>
            </select>   
        </div>          
        <div class="clear"></div>       
         <div class="dual-input">
        	<p class="input-top-p admin-top-p">Pet Breed*</p>
        	<select class="input-name clean admin-input" required >
            	<option>Reptile Breed 1</option>
                <option>Reptile Breed 2</option>
            </select>       
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Seller*</p>
        	<select class="input-name clean admin-input" required >
            	<option>Fur</option>
                <option>Paw</option>
            </select>   
        </div>         
        <div class="clear"></div>
        <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Details</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Details" name="">           
        </div>
        <div class="clear"></div>
        <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Video Link*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Video Link" name=""  required>           
        </div>
        <div class="clear"></div>  
		<div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Upload Pet Photo X4*</p>
            <!-- Photo cropping into square size feature -->
        </div>             
        <div class="clear"></div>
    	

        <div class="clear"></div>  
        <div class="width100 overflow text-center">     
        	<button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button>
        </div>
        </form>
	</div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>