<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit User Details | Mypetslibrary" />
<title>Edit User Details | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'userHeaderAfterLogin.php'; ?>
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
            <h1 class="green-text h1-title">Edit User Details</h1>
            <div class="green-border"></div>
   </div>
   <div class="border-separation">
        <div class="clear"></div>
 		<form>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Userame*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Username" required name="" value="">      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Email*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Email" required name="" value="">     
        </div>        
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Facebook ID (Optional)</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Facebook ID (Optional)"  required name="" value="">      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Contact No.*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Contact No."  required name="" value="">    
        </div>   
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Birthday</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Birthday" name="" value="">      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Gender</p>
        	<select class="input-name clean admin-input" required >
            	<option>Male</option>
                <option>Female</option>
            </select>   
        </div>           
		<div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Account Status</p>
        	<select class="input-name clean admin-input" required >
            	<option>Active</option>
                <option>Banned</option>
            </select>   
        </div>   
        <div class="clear"></div>  
		<div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Upload Profile Picture</p>
            <!-- Photo cropping into square size feature -->
        </div>             
        <div class="clear"></div>
    	<p class="review-product-name">Shipping Details</p>        
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Receiver Name</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Receiver Name" name="" value="">    
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Receiver Contact No.</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Receiver Contact Number" name="" value="">    
        </div>        
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">State</p>
        	<select class="input-name clean admin-input">
            	<option>State</option>
                <option>Penang</option>
            </select>      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Area</p>
        	<select class="input-name clean admin-input">
            	<option>Area</option>
                <option>Bayan Baru</option>
            </select>       
        </div>         
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Postal Code</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Postal Code" name="" value="">    
        </div> 
   
        <div class="clear"></div>                
        <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Shipping Address</p>
        	<textarea class="input-name clean input-textarea address-textarea admin-address-textarea" type="text" placeholder="Shipping Address" name="" value=""></textarea>
        </div>
        <div class="clear"></div>
    	<p class="review-product-name">Bank Account Details 1</p> 
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Bank</p>
        	<select class="input-name clean admin-input" required >
            	<option>Bank</option>
                <option>Maybank</option>
                <option>CIMB</option>
            </select>     
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Bank Account Holder Name</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Bank Account Holder Name" required name="" value="">      
        </div>        
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Bank Account No.</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Bank Account No." required name="" value="">         
        </div>
        <div class="clear"></div>
    	<p class="review-product-name">Credit/Debit Card 1</p>

        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Name on Card</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Name on Card" required name="" value="">    
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Card Number</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Card Number" required name="" value="">    
        </div>        
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Card type</p>
        	<select class="input-name clean admin-input" required >
            	<option>Card Type</option>
                <option>Credit</option>
                <option>Debit</option>
            </select>      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Expiry Date</p>
        	<input class="input-name clean input-textarea admin-input" type="date" placeholder="Expiry Date" required name="" value="">          
        </div>         
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">CCV</p>
 
            	<input class="input-name clean input-password edit-password-input admin-input"  type="text" placeholder="CCV" required name="" value="">
                <p class="edit-p-password open-ccv"><img src="img/ccv.png" class="hover1a edit-password-img ccv-img" alt="CCV" title="CCV"><img src="img/ccv2.png" class="hover1b edit-password-img ccv-img" alt="CCV" title="CCV"></p>
                
        </div> 
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Postal Code</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Postal Code" required name="" value="">          
        </div>         
        <div class="clear"></div>
        <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Billing Address</p>
        	<textarea class="input-name clean address-textarea admin-address-textarea" type="text" placeholder="Billing Address" required name=""></textarea>      
        </div>         

        <div class="clear"></div>  
        <div class="width100 overflow text-center">     
        	<button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button>
        </div>
        </form>
	</div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>