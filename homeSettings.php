<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Homepage Settings | Mypetslibrary" />
<title>Homepage Settings | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance">
	<h1 class="green-text h1-title">Homepage Settings</h1>
	<div class="green-border"></div>
    <div class="clear"></div>
    <div class="width100 border-separation">
    	<a href="slider.php" class="opacity-hover">
            <div class="white-dropshadow-box four-div-box">
                <img src="img/pet-photo2.png" alt="Slider" title="Slider" class="four-div-img">
                <p class="four-div-p"><b>Slider</b></p>
            </div>
        </a>
        <a href="featuredPets.php" class="opacity-hover">
            <div class="white-dropshadow-box four-div-box second-four-div-box left-four-div">
                <img src="img/pet-food.png" alt="Featured Products" title="Featured Products" class="four-div-img">
                <p class="four-div-p"><b>Featured Products</b></p>
            </div> 
        </a>
        <a class="opacity-hover">
            <div class="white-dropshadow-box four-div-box right-four-div">
                <img src="img/cute-dog.png" alt="Featured Pets" title="Featured Pets" class="four-div-img">
                <p class="four-div-p"><b>Featured Pets</b></p>
            </div>  
        </a>
        <a class="opacity-hover">       
            <div class="white-dropshadow-box four-div-box second-four-div-box forth-div">
                <img src="img/pet-business.png" alt="Featured Partners" title="Featured Partners" class="four-div-img">
                <p class="four-div-p"><b>Featured Partners</b></p>
            </div>  
        </a>
        </div>
        <div class="clear"></div>      


</div>
<div class="clear"></div>


<?php include 'js.php'; ?>





</body>
</html>