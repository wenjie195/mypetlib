<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Online Pet Store | Mypetslibrary" />
<title>Online Pet Store | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
  <link rel="stylesheet" type="text/css" href="./slick/slick.css">
  <link rel="stylesheet" type="text/css" href="./slick/slick-theme.css">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
 
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<?php include 'userHeader.php'; ?>
    <div id="jssor_1" style="position:relative;left:0px;width:1300px;height:500px;overflow:hidden;visibility:hidden;" class="menu-distance">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="../svg/loading/static-svg/spin.svg" />
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:500px;overflow:hidden;">
            <div>
                <img data-u="image" src="img/poster1.jpg" />
            </div>
            <div>
                <img data-u="image" src="img/poster2.jpg" />
            </div>
            <div>
                <img data-u="image" src="img/poster3.jpg" />
            </div>
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb032" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
            <div data-u="prototype" class="i" style="width:16px;height:16px;">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                </svg>
            </div>
        </div>
        <!-- Arrow Navigator -->
        <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
            </svg>
        </div>
    </div>

<div class="clear"></div>
<div class="overflow grey-bg">
    <div class="width100 small-padding">
    <h1 class="green-text user-title left-align-title margin-bottom-5px">Puppy</h1>
    <a href="malaysia-cute-puppy-dog.php" class="right-align-link view-a light-green-a hover-a">View More</a>
    </div>
    <div class="clear"></div>
    <div class="width100 small-padding">
  	<section class="variable slider ow-homepage-slider">
  	<!--<div class="overbigflow">-->
        <a href="puppyDetails.php">
            <div class="shadow-white-box product-box featured">
                  <div class="width100 white-bg">
                    <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                        <p class="slider-product-price">RM5XXX</p>
                        <p class="width100 text-overflow slider-location">Penang</p>
                  </div>
            </div>
        </a>
        <a href="puppyDetails.php">
            <div class="shadow-white-box product-box featured">
              <div class="width100 white-bg">
                <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
              </div>
              <div class="width100 product-details-div">
                    <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                    <p class="slider-product-price">RM5XXX</p>
                    <p class="width100 text-overflow slider-location">Penang</p>
              </div>
            </div>
        </a>
        <a href="puppyDetails.php">
            <div class="shadow-white-box product-box featured">
              <div class="width100 white-bg">
                <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
              </div>
              <div class="width100 product-details-div">
                    <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                    <p class="slider-product-price">RM5XXX</p>
                    <p class="width100 text-overflow slider-location">Penang</p>
              </div>
            </div>
        </a>
        <a href="puppyDetails.php">
            <div class="shadow-white-box product-box featured">
              <div class="width100 white-bg">
                <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
              </div>
              <div class="width100 product-details-div">
                    <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                    <p class="slider-product-price">RM5XXX</p>
                    <p class="width100 text-overflow slider-location">Penang</p>
              </div>
            </div>
        </a>
        <a href="puppyDetails.php">
            <div class="shadow-white-box product-box featured">
              <div class="width100 white-bg">
                <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
              </div>
              <div class="width100 product-details-div">
                    <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                    <p class="slider-product-price">RM5XXX</p>
                    <p class="width100 text-overflow slider-location">Penang</p>
              </div>
            </div>
        </a>
        <a href="puppyDetails.php">
            <div class="shadow-white-box product-box featured">
              <div class="width100 white-bg">
                <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
              </div>
              <div class="width100 product-details-div">

                    <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                    <p class="slider-product-price">RM5XXX</p>
                    <p class="width100 text-overflow slider-location">Penang</p>
              </div>
            </div>
        </a>
        <a href="puppyDetails.php">
            <div class="shadow-white-box product-box featured">
              <div class="width100 white-bg">
                <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
              </div>
              <div class="width100 product-details-div">
                    <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                    <p class="slider-product-price">RM5XXX</p>
                    <p class="width100 text-overflow slider-location">Penang</p>
              </div>
            </div>   
        </a>
        <a href="puppyDetails.php"> 
            <div class="shadow-white-box product-box featured">
              <div class="width100 white-bg">
                <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
              </div>
              <div class="width100 product-details-div">
                    <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                    <p class="slider-product-price">RM5XXX</p>
                    <p class="width100 text-overflow slider-location">Penang</p>
              </div>
            </div>
        </a>
        <a href="puppyDetails.php">
            <div class="shadow-white-box product-box featured">
              <div class="width100 white-bg">
                <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
              </div>
              <div class="width100 product-details-div">
                    <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                    <p class="slider-product-price">RM5XXX</p>
                    <p class="width100 text-overflow slider-location">Penang</p>
              </div>
            </div>
        </a>
        <a href="puppyDetails.php">
            <div class="shadow-white-box product-box featured">
              <div class="width100 white-bg">
                <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
              </div>
              <div class="width100 product-details-div">
                    <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                    <p class="slider-product-price">RM5XXX</p>
                    <p class="width100 text-overflow slider-location">Penang</p>
              </div>
            </div> 
        </a>  
         <!--</div>-->     
  </section>
    
        
    
    </div>
</div>
<div class="clear"></div>
<div class="overflow">
    <div class="width100 small-padding">
    <h1 class="green-text user-title left-align-title margin-bottom-5px">Kitten</h1>
    <a href="malaysia-cute-kitten-cat.php" class="right-align-link view-a light-green-a hover-a">View More</a>
    </div>
    <div class="clear"></div>
    <div class="width100 small-padding">
  	<section class="variable slider ow-homepage-slider">
  	<!--<div class="overbigflow">-->
        <a href="puppyDetails.php">
            <div class="shadow-white-box product-box featured">
                  <div class="width100 white-bg">
                    <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                        <p class="slider-product-price">RM5XXX</p>
                        <p class="width100 text-overflow slider-location">Penang</p>
                  </div>
            </div>
        </a>
        <a href="puppyDetails.php">
            <div class="shadow-white-box product-box featured">
              <div class="width100 white-bg">
                <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
              </div>
              <div class="width100 product-details-div">
                    <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                    <p class="slider-product-price">RM5XXX</p>
                    <p class="width100 text-overflow slider-location">Penang</p>
              </div>
            </div>
        </a>
        <a href="puppyDetails.php">
            <div class="shadow-white-box product-box featured">
              <div class="width100 white-bg">
                <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
              </div>
              <div class="width100 product-details-div">
                    <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                    <p class="slider-product-price">RM5XXX</p>
                    <p class="width100 text-overflow slider-location">Penang</p>
              </div>
            </div>
        </a>
        <a href="puppyDetails.php">
            <div class="shadow-white-box product-box featured">
              <div class="width100 white-bg">
                <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
              </div>
              <div class="width100 product-details-div">
                    <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                    <p class="slider-product-price">RM5XXX</p>
                    <p class="width100 text-overflow slider-location">Penang</p>
              </div>
            </div>
        </a>
        <a href="puppyDetails.php">
            <div class="shadow-white-box product-box featured">
              <div class="width100 white-bg">
                <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
              </div>
              <div class="width100 product-details-div">
                    <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                    <p class="slider-product-price">RM5XXX</p>
                    <p class="width100 text-overflow slider-location">Penang</p>
              </div>
            </div>
        </a>
        <a href="puppyDetails.php">
            <div class="shadow-white-box product-box featured">
              <div class="width100 white-bg">
                <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
              </div>
              <div class="width100 product-details-div">

                    <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                    <p class="slider-product-price">RM5XXX</p>
                    <p class="width100 text-overflow slider-location">Penang</p>
              </div>
            </div>
        </a>
        <a href="puppyDetails.php">
            <div class="shadow-white-box product-box featured">
              <div class="width100 white-bg">
                <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
              </div>
              <div class="width100 product-details-div">
                    <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                    <p class="slider-product-price">RM5XXX</p>
                    <p class="width100 text-overflow slider-location">Penang</p>
              </div>
            </div>   
        </a>
        <a href="puppyDetails.php"> 
            <div class="shadow-white-box product-box featured">
              <div class="width100 white-bg">
                <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
              </div>
              <div class="width100 product-details-div">
                    <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                    <p class="slider-product-price">RM5XXX</p>
                    <p class="width100 text-overflow slider-location">Penang</p>
              </div>
            </div>
        </a>
        <a href="puppyDetails.php">
            <div class="shadow-white-box product-box featured">
              <div class="width100 white-bg">
                <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
              </div>
              <div class="width100 product-details-div">
                    <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                    <p class="slider-product-price">RM5XXX</p>
                    <p class="width100 text-overflow slider-location">Penang</p>
              </div>
            </div>
        </a>
        <a href="puppyDetails.php">
            <div class="shadow-white-box product-box featured">
              <div class="width100 white-bg">
                <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
              </div>
              <div class="width100 product-details-div">
                    <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                    <p class="slider-product-price">RM5XXX</p>
                    <p class="width100 text-overflow slider-location">Penang</p>
              </div>
            </div> 
        </a>  
         <!--</div>-->     
  </section>
    
        
    
    </div>
</div>
<div class="clear"></div>
<div class="overflow grey-bg">
    <div class="width100 small-padding">
    <h1 class="green-text user-title left-align-title margin-bottom-5px">Products</h1>
    <a class="right-align-link view-a light-green-a hover-a" href="malaysia-pet-food-toy-product.php">View More</a>
    </div>
    <div class="clear"></div>
    <div class="width100 small-padding grey-bg">
      <section class="variable slider ow-homepage-slider">
        <!--<div class="overbigflow">-->
            <a href="productDetails.php">
                <div class="shadow-white-box product-box">
                  <div class="width100">
                    <img src="img/product.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Product Name Product Name Product Name Product Name</p>
                        <p class="slider-product-price">RM22.00</p>
                  </div>
                </div>
            </a>
            <a href="productDetails.php">
                <div class="shadow-white-box product-box">
                  <div class="width100">
                    <img src="img/product.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Product Name Product Name Product Name Product Name</p>
                        <p class="slider-product-price">RM22.00</p>
                  </div>
                </div>
            </a>
            <a href="productDetails.php">
                <div class="shadow-white-box product-box">
                  <div class="width100">
                    <img src="img/product.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Product Name Product Name Product Name Product Name</p>
                        <p class="slider-product-price">RM22.00</p>
                  </div>
                </div>
            </a>
            <a href="productDetails.php">
                <div class="shadow-white-box product-box">
                  <div class="width100">
                    <img src="img/product.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Product Name Product Name Product Name Product Name</p>
                        <p class="slider-product-price">RM22.00</p>
                  </div>
                </div>
            </a>
            <a href="productDetails.php">
                <div class="shadow-white-box product-box">
                  <div class="width100">
                    <img src="img/product.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Product Name Product Name Product Name Product Name</p>
                        <p class="slider-product-price">RM22.00</p>
                  </div>
                </div>
            </a>
            <a href="productDetails.php">
                <div class="shadow-white-box product-box">
                  <div class="width100">
                    <img src="img/product.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Product Name Product Name Product Name Product Name</p>
                        <p class="slider-product-price">RM22.00</p>
                  </div>
                </div>
            </a>
            <a href="productDetails.php">
                <div class="shadow-white-box product-box">
                  <div class="width100">
                    <img src="img/product.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Product Name Product Name Product Name Product Name</p>
                        <p class="slider-product-price">RM22.00</p>
                  </div>
                </div>   
            </a>
            <a href="productDetails.php"> 
                <div class="shadow-white-box product-box">
                  <div class="width100">
                    <img src="img/product.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Product Name Product Name Product Name Product Name</p>
                        <p class="slider-product-price">RM22.00</p>
                  </div>
                </div>
            </a>
            <a href="productDetails.php">
                <div class="shadow-white-box product-box">
                  <div class="width100">
                    <img src="img/product.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Product Name Product Name Product Name Product Name</p>
                        <p class="slider-product-price">RM22.00</p>
                  </div>
                </div>
            </a>
            <a href="productDetails.php">
                <div class="shadow-white-box product-box">
                  <div class="width100">
                    <img src="img/product.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Product Name Product Name Product Name Product Name</p>
                        <p class="slider-product-price">RM22.00</p>
                  </div>
               
              </div> 
            </a>  
             <!--</div>-->     
      </section>
        
    
    </div>
</div>
<div class="clear"></div>

<div class="width100 same-padding">
	<h1 class="green-text user-title left-align-title">Our Partners</h1>
	<div class="clear"></div>
    <div class="width100">
    	<a href="petSellerDetails.php" class="opacity-hover"><img src="img/partner1.png" alt="Partner Name" title="Partner" class="partner-img"></a>
        <a href="petSellerDetails.php" class="opacity-hover"><img src="img/partner2.png" alt="Partner Name" title="Partner" class="partner-img second-partner-img"></a>
    	<a href="petSellerDetails.php" class="opacity-hover"><img src="img/partner3.png" alt="Partner Name" title="Partner" class="partner-img"></a>
        <a href="petSellerDetails.php" class="opacity-hover"><img src="img/partner4.png" alt="Partner Name" title="Partner" class="partner-img second-partner-img"></a>
        <a href="petSellerDetails.php" class="opacity-hover"><img src="img/partner5.png" alt="Partner Name" title="Partner" class="partner-img"></a>
    </div>
    <div class="clear"></div>
    <a href="pet-seller-grooming-delivery-hotel.php"><div class="green-button mid-btn-width">View All</div></a>
    <div class="clear"></div>
    <div class="mini-height30 width100"></div>
</div>
<div class="clear"></div>
<div class="width100 same-padding adopt-bg">
    <div class="left-adopt-div">
    	<h1 class="adopt-h1">Adoption <img src="img/paw.png" class="paw-png" alt="Pet Adoption" title="Pet Adoption"></h1>
        <p class="adopt-p adopt1">Adopt a pet and give it a home,<br>it will be love you back unconditionally.</p>
        <p class="adopt-p adopt2">Adopt a pet and give it a home, it will be love you back unconditionally.</p>
        <a href="malaysia-animal-pet-adoption.php"><div class="adopt-btn">Adopt a Pet</div></a>
    </div>



</div>
<div class="clear"></div>
<div class="grey-bg width100 same-padding overflow">
    <h1 class="green-text user-title left-align-title">Blog</h1>
    <a href="malaysia-pet-blog.php" class="right-align-link view-a light-green-a hover-a">View More</a>
	<div class="clear"></div>
    <a href="pet-blog.php" class="opacity-hover">
        <div class="two-div-width shadow-white-box blog-box opacity-hover">
            <div class="left-img-div2">
            	<img src="img/blog-picture.jpg" class="width100" alt="Blog Title" title="Blog Title">
            </div>
            <div class="right-content-div3">
                <h3 class="article-title text-overflow">
                    Think Thrice Before Getting a Pet
                </h3>
                <p class="date-p">
                    29/11/2019
                </p>
                <p class="right-content-p">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.
                </p>
            </div>
        </div>
    </a>
    <a href="pet-blog.php" class="opacity-hover">
        <div class="two-div-width shadow-white-box second-two-div-width blog-box opacity-hover">
            <div class="left-img-div2">
            	<img src="img/blog-picture.jpg" class="width100" alt="Blog Title" title="Blog Title">
            </div>
            <div class="right-content-div3">
                <h3 class="article-title text-overflow">
                    The Standing Trick Tutorial
                </h3>
                <p class="date-p">
                    29/11/2019
                </p>
                <p class="right-content-p">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.
                </p>
            </div>
        </div>
	</a>
    <div class="clear"></div>
    <a href="pet-blog.php" class="opacity-hover">
        <div class="two-div-width shadow-white-box blog-box opacity-hover">
            <div class="left-img-div2">
            	<img src="img/blog-picture.jpg" class="width100" alt="Blog Title" title="Blog Title">
            </div>
            <div class="right-content-div3">
                <h3 class="article-title text-overflow">
                    Think Thrice Before Getting a Pet
                </h3>
                <p class="date-p">
                    29/11/2019
                </p>
                <p class="right-content-p">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.
                </p>
            </div>
        </div>
    </a>
    <a href="pet-blog.php" class="opacity-hover">
        <div class="two-div-width shadow-white-box second-two-div-width blog-box opacity-hover">
            <div class="left-img-div2">
            	<img src="img/blog-picture.jpg" class="width100" alt="Blog Title" title="Blog Title">
            </div>
            <div class="right-content-div3">
                <h3 class="article-title text-overflow">
                    The Standing Trick Tutorial
                </h3>
                <p class="date-p">
                    29/11/2019
                </p>
                <p class="right-content-p">
                   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.
                </p>
            </div>
        </div>
	</a>    
</div>
<div class="clear"></div>
<div class="width100 same-padding overflow">
    <h1 class="green-text user-title left-align-title">Contact Us</h1>
  <a href="contactus.php" class="right-align-link view-a light-green-a hover-a">View More</a>
	<div class="clear"></div>
	<div class="width100 text-center">
   	  <img src="img/mypetslibrary2.png" class="middle-logo" alt="Mypetslibrary" title="Mypetslibrary">
    </div>
  <div class="clear"></div>
    <p class="center-content">
    	Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide.
        <br>
        Email: <b>mypetslibrary@gmail.com</b> 
    </p>
    <div class="contact-div">
        <form id="contactform" method="post" action="index.php" class="form-class extra-margin">
                      
                      <input type="text" name="name" placeholder="Name" class="input-name clean" required >
                      
                      <input type="email" name="email" placeholder="Email" class="input-name clean" required >                  
                      
                      <input type="text" name="telephone" placeholder="Contact Number" class="input-name clean" required >
    
    
                                      
                      <textarea name="comments" placeholder="Message" class="input-name input-message clean" ></textarea>
                      <div class="clear"></div>
                      <input type="radio" name="contact-option" value="contact-more-info" class="radio1 float-left clean" required><p class="opt-msg left"> I want to be updated with more information about your company's news and future promotions</p>
                      <div class="clear"></div>
                      <input type="radio" name="contact-option" value="contact-on-request" class="radio1 float-left clean"  required><p class="opt-msg left"> I just want to be contacted based on my request/inquiry</p>
                      <div class="clear"></div>
                       <div class="width100 text-center">
                      <input type="submit" name="send_email_button" value="SEND US A MESSAGE" class="form-submit mid-btn-width green-button white-text clean pointer">
                      </div>
                </form>   
                <div class="width100 bottom-spacing"></div> 
                <div class="clear"></div>
    </div>
</div>
<div class="clear"></div>
<p class="privacy-p"><a href="privacypolicy.php" class="dark-green-a">Privacy Policy</a> | <a href="termsandconditions.php" class="dark-green-a">Terms and Conditions</a></p>

  <style type="text/css">

    * {
      box-sizing: border-box;
    }

    .slider {
        width: 50%;
        margin: 100px auto;
    }

    .slick-slide {
      margin: 0px 12px;
    }

    .slick-slide img {
      width: 100%;
    }

    .slick-prev:before,
    .slick-next:before {
      color: black;
    }




    .slick-current {
      opacity: 1;
    }
	.slick-slide:foucs{
	outline:none !important;
	}
	div{outline:none !important;}


	.home-a .hover1a{
		display:none !important;}
	.home-a .hover1b{
		display:inline-block !important;}
		
	@media (max-width: 800px){
	.slick-slide {
		margin: 0px 8px;
	}		
	}
  </style>
<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>
<?php

if( $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['send_email_button'])) {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "sherry2.vidatech@gmail.com";
    $email_subject = "Contact Form via Mypetslibrary website";
 
    function died($error) 
	{
        // your error code can go here
		echo '<script>alert("We are very sorry, but there were error(s) found with the form you submitted.\n\nThese errors appear below.\n\n';
		echo $error;
        echo '\n\nPlease go back and fix these errors.\n\n")</script>';
        die();
    }
 
 
    // validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['email']) ||
		!isset($_POST['telephone'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
	
     
 
    $first_name = $_POST['name']; // required
    $email_from = $_POST['email']; // required
	$telephone = $_POST['telephone']; //required
    $comments = $_POST['comments']; 
    $contactOption = $_POST['contact-option']; // required
    $contactMethod = null;
	
	//$error_message = '<script>alert("The name you entered does not appear to be valid.");</script>';
	//if($first_name == ""){
	//	echo $error_message;
	//}

    if($contactOption == null || $contactOption == ""){
        $contactMethod = "don\'t bother me";
    }else if($contactOption == "contact-more-info"){
        $contactMethod = "I want to be contacted with more information about your company's offering marketing services and consulting";
    }else if($contactOption == "contact-on-request"){
        $contactMethod = "I just want to be contacted based on my request/ inquiry";
    }else{
        $contactMethod = "error getting contact options";
		$error_message .="Error getting contact options\n\n";
    }

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The email address you entered does not appear to be valid.\n';
  }
 
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The name you entered does not appear to be valid.\n';
  }
 


 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
    $email_message = "Form details below.\n\n";
 
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
 
    $email_message .= "Name: ".clean_string($first_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
	$email_message .= "Telephone: ".clean_string($telephone)."\n";   
    $email_message .= "Message : ".clean_string($comments)."\n";
    $email_message .= "Contact Option : ".clean_string($contactMethod)."\n";

// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);  
echo '<script>alert("Thank you! We will be in contact shortly!")</script>';

?>
<!-- include your own success html here -->

<!--Thank you for contacting us. We will be in touch with you very soon.-->
<?php
 
}
?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully registered new user!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no referrer with this email ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "User password must be more than 5 !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "User password does not match";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Registration of new user failed!";
        } 
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>
</body>
</html>