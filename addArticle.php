<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add Article | Mypetslibrary" />
<title>Add Article | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'userHeaderAfterLogin.php'; ?>
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
            <h1 class="green-text h1-title">Add New Article</h1>
            <div class="green-border"></div>
   </div>
   <div class="border-separation">
        <div class="clear"></div>
 		<form>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Title*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Title" required name="">      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Status*</p>
        	<select class="input-name clean admin-input" required >
            	<option>Publish/Approved</option>
                <option>Not Available</option>
            </select>     
        </div>        
        <div class="clear"></div>
      	<div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Article Slug/Link (or URL, Avoid Spacing and Symbol Sspecially"',) Can Use - *</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Article Slug/Link" required name="">              	
        </div>
        <div class="clear"></div>
      	<div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Keyword (Use Coma , to Separate Each Keyword)</p>
        	<textarea class="input-name clean input-textarea admin-input keyword-input" type="text" placeholder="cute,malaysia,pet,dog,puppy," required name=""></textarea>  	
        </div>        
        <div class="clear"></div>  
        <div class="width100 overflow text-center">     
        	<button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button>
        </div>
        </form>
	</div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>