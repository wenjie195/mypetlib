<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Refund | Mypetslibrary" />
<title>Refund | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
 
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
    <h1 class="green-text user-title left-align-title opacity-hover" onclick="goBack()"><img src="img/back2.png" class="back-button" alt="Back" title="Back"> Refund</h1>
    <div class="clear"></div>
    <div class="dual-div">
    	<p class="green-text top-text">Order No.</p>
        <p class="bottom-text">11334235</p>
    </div>
    <div class="dual-div second-dual-div">
    	<p class="green-text top-text">Paid On</p>
        <p class="bottom-text">14/01/20</p>
    </div> 
    <div class="clear"></div>
    <div class="width100 overflow">
    	<p class="green-text top-text">Ordered Products</p>
        <div class="table-scroll-div">
            <table class="order-table">
                <thead>	
                    <tr>
                        <th>No.</th>
                        <th>Product</th>
                        <th>Qty</th>
                        <th class="price-column">Price (RM)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1.</td>
                        <td>Pedigree Dentastix Puppy 56g Dog Treats</td>
                        <td>1</td>
                        <td class="price-column">6.67</td>
                    </tr>
                </tbody>
            </table>
    	</div>
        <table class="price-table">
        	<tbody>
                <tr>
                	<td>Subtotal</td>
                    <td class="price-padding">RM6.67</td>
                </tr>
                <tr>
                	<td>Shipping</td>
                    <td class="price-padding">RM0.00</td>
                </tr>
                <tr>
                	<td>Discount</td>
                    <td class="price-padding">RM0.00</td>
                </tr>
                <tr>
                	<td class="last-td"><b>Total</b></td>
                    <td class="last-td price-padding"><b>RM6.67</b></td>
                </tr>                                                
            </tbody>
        </table>
        </div>
        <div class="clear"></div>
        <div class="width100 overflow some-margin-bottom">
            <p class="green-text top-text">Delivery Address</p>
            <p class="bottom-text">Alice Tan Ai Ling</p>  
            <p class="bottom-text">(+60) 1012345678</p>
            <p class="bottom-text">No., Street, City, Postcode, State, Country</p>      	
        </div>

        
        
        
        <div class="clear"></div>
        <div class="dual-div">
            <p class="green-text top-text">Tracking No.</p>
            <p class="bottom-text">My10298373889 <button class="clean transparent-button" id="copy-referral-link"><img src="img/copy.png" class="copy-png" alt="Copy" title="Copy"></button></p>
            <input type="hidden" id="linkCopy" value="My10298373889">
        </div>
        <div class="dual-div second-dual-div">
            <p class="green-text top-text">Shipping Method</p>
            <p class="bottom-text">Pos Laju</p>
        </div>         
        <div class="clear"></div>
        <div class="dual-div">
            <p class="green-text top-text">Shipping Status</p>
            <p class="bottom-text">Completed</p>
        </div>
        <div class="dual-div second-dual-div">
            <p class="green-text top-text">Delivered On</p>
            <p class="bottom-text">15/02/20</p>
        </div>  
        <div class="clear"></div>
        
        <div class="width100 overflow some-margin-bottom">
        	<p class="green-text top-text">Reason</p>
            <p class="bottom-text">The product is expired.</p>
            <div class="width100 overflow">
            <a href="./img/product-1.jpg" data-fancybox="images-preview"  >
            	<img src="img/product-1.jpg" class="refund-product opacity-hover" >
            </a>
            </div>
        </div>        
        <div class="clear"></div>
        
        <div class="dual-div">
            <p class="green-text top-text">Bank</p>
            <p class="bottom-text">Maybank - 55511334235</p>
        </div>
        <div class="dual-div second-dual-div">
            <p class="green-text top-text">Bank Account Holder Name</p>
            <p class="bottom-text">Alice Tan Ai Ling</p>
        </div> 
        <div class="clear"></div>        
   		<div class="width100 overflow some-margin-bottom">
        	<p class="green-text top-text">Your Decision</p>
            <select class="input-name clean admin-input" required >
            	<option>Agree and Refund</option>
                <option>Reject</option>
            </select>           
        </div>
        <div class="clear"></div>        
   		<div class="width100 overflow some-margin-bottom">
        	<p class="green-text top-text">Remark</p>
            <input class="input-name clean input-textarea admin-input date-input" type="text" placeholder="Remark" name="" value="">           
        </div>   
        <div class="clear"></div>
   		<div class="width100 overflow some-margin-bottom">        
        	<p class="green-text top-text">Upload Refund Receipt (Optional)</p>
        </div>
        <div class="clear"></div>
        <div class="width100 overflow text-center">     
        	<button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button>
        </div>
        </form>            
</div>
<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>