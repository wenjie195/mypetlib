<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Malaysia Pet Food Toy Product | Mypetslibrary" />
<title>Malaysia Pet Food Toy Product | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="pet food, pet grooming, pet shampoo, toy for pet, pet product, Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'userHeaderAfterLogin.php'; ?>
	<div class="fix-filter width100  small-padding overflow product-filter-white-div">
        <h1 class="green-text user-title left-align-title product-filter-h1">Product</h1>
        <a class="open-productfilter filter-a green-a product-filter-a">Filter</a>
        <div class="filter-div product-filter-div">
            <button class="transparent-button filter-btn clean grey-to-green product-btn">Favourite</button>
            <button class="transparent-button filter-btn clean grey-to-green product-btn">Top Sale</button>
            <button class="transparent-button filter-btn clean grey-to-green product-btn">Free Gift</button> 
            <button class="transparent-button filter-btn clean grey-to-green product-btn">Price</button>
            
        </div>
        
    </div>

	<div class="clear"></div>    
<div class="width100  small-padding overflow min-height-with-filter filter-distance product-filter-distance">

	<div class="width103 product-big-div">
        <a href="productDetails.php">
            <div class="shadow-white-box featured four-box-size">
                  <div class="width100 white-bg">
                    <img src="img/product-img.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Pedigree Dentastix Dog Treats</p>
                        <p class="product-price-width text-overflow slider-product-price left-price">RM2800.00</p>
                        <p class="slider-product-price right-like hover1">
                        	<img src="img/favourite-1.png" class="hover1a like-img width100" alt="Favourite" title="Favourite">
                            <img src="img/favourite-2.png" class="hover1b like-img width100" alt="Favourite" title="Favourite">
                        </p>
                        <div class="clear"></div>
                        <p class="left-rating">5<img src="img/yellow-star.png" class="rating-tiny-star" alt="Rating" title="Rating"></p>
                        <p class="right-sold">1000 <span class="sold-color">Sold</span></p>
                  </div>
            </div>
        </a> 
        <a href="productDetails.php">
            <div class="shadow-white-box featured four-box-size">
                  <div class="width100 white-bg">
                    <img src="img/product-img.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Pedigree Dentastix Dog Treats</p>
                        <p class="product-price-width text-overflow slider-product-price left-price">RM2800.00</p>
                        <p class="slider-product-price right-like hover1">
                        	<img src="img/favourite-1.png" class="hover1a like-img width100" alt="Favourite" title="Favourite">
                            <img src="img/favourite-2.png" class="hover1b like-img width100" alt="Favourite" title="Favourite">
                        </p>
                        <div class="clear"></div>
                        <p class="left-rating">5<img src="img/yellow-star.png" class="rating-tiny-star" alt="Rating" title="Rating"></p>
                        <p class="right-sold">1000 <span class="sold-color">Sold</span></p>
                  </div>
            </div>
        </a>        
        <a href="productDetails.php">
            <div class="shadow-white-box featured four-box-size">
                  <div class="width100 white-bg">
                    <img src="img/product-img.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Pedigree Dentastix Dog Treats</p>
                        <p class="product-price-width text-overflow slider-product-price left-price">RM2800.00</p>
                        <p class="slider-product-price right-like hover1">
                        	<img src="img/favourite-1.png" class="hover1a like-img width100" alt="Favourite" title="Favourite">
                            <img src="img/favourite-2.png" class="hover1b like-img width100" alt="Favourite" title="Favourite">
                        </p>
                        <div class="clear"></div>
                        <p class="left-rating">5<img src="img/yellow-star.png" class="rating-tiny-star" alt="Rating" title="Rating"></p>
                        <p class="right-sold">1000 <span class="sold-color">Sold</span></p>
                  </div>
            </div>
        </a> 
        <a href="productDetails.php">
            <div class="shadow-white-box featured four-box-size">
                  <div class="width100 white-bg">
                    <img src="img/product-img.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Pedigree Dentastix Dog Treats</p>
                        <p class="product-price-width text-overflow slider-product-price left-price">RM2800.00</p>
                        <p class="slider-product-price right-like hover1">
                        	<img src="img/favourite-1.png" class="hover1a like-img width100" alt="Favourite" title="Favourite">
                            <img src="img/favourite-2.png" class="hover1b like-img width100" alt="Favourite" title="Favourite">
                        </p>
                        <div class="clear"></div>
                        <p class="left-rating">5<img src="img/yellow-star.png" class="rating-tiny-star" alt="Rating" title="Rating"></p>
                        <p class="right-sold">1000 <span class="sold-color">Sold</span></p>
                  </div>
            </div>
        </a>         
        <a href="productDetails.php">
            <div class="shadow-white-box featured four-box-size">
                  <div class="width100 white-bg">
                    <img src="img/product-img.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Pedigree Dentastix Dog Treats</p>
                        <p class="product-price-width text-overflow slider-product-price left-price">RM2800.00</p>
                        <p class="slider-product-price right-like hover1">
                        	<img src="img/favourite-1.png" class="hover1a like-img width100" alt="Favourite" title="Favourite">
                            <img src="img/favourite-2.png" class="hover1b like-img width100" alt="Favourite" title="Favourite">
                        </p>
                        <div class="clear"></div>
                        <p class="left-rating">5<img src="img/yellow-star.png" class="rating-tiny-star" alt="Rating" title="Rating"></p>
                        <p class="right-sold">1000 <span class="sold-color">Sold</span></p>
                  </div>
            </div>
        </a> 
        <a href="productDetails.php">
            <div class="shadow-white-box featured four-box-size">
                  <div class="width100 white-bg">
                    <img src="img/product-img.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Pedigree Dentastix Dog Treats</p>
                        <p class="product-price-width text-overflow slider-product-price left-price">RM2800.00</p>
                        <p class="slider-product-price right-like hover1">
                        	<img src="img/favourite-1.png" class="hover1a like-img width100" alt="Favourite" title="Favourite">
                            <img src="img/favourite-2.png" class="hover1b like-img width100" alt="Favourite" title="Favourite">
                        </p>
                        <div class="clear"></div>
                        <p class="left-rating">5<img src="img/yellow-star.png" class="rating-tiny-star" alt="Rating" title="Rating"></p>
                        <p class="right-sold">1000 <span class="sold-color">Sold</span></p>
                  </div>
            </div>
        </a>        
        <a href="productDetails.php">
            <div class="shadow-white-box featured four-box-size">
                  <div class="width100 white-bg">
                    <img src="img/product-img.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Pedigree Dentastix Dog Treats</p>
                        <p class="product-price-width text-overflow slider-product-price left-price">RM2800.00</p>
                        <p class="slider-product-price right-like hover1">
                        	<img src="img/favourite-1.png" class="hover1a like-img width100" alt="Favourite" title="Favourite">
                            <img src="img/favourite-2.png" class="hover1b like-img width100" alt="Favourite" title="Favourite">
                        </p>
                        <div class="clear"></div>
                        <p class="left-rating">5<img src="img/yellow-star.png" class="rating-tiny-star" alt="Rating" title="Rating"></p>
                        <p class="right-sold">1000 <span class="sold-color">Sold</span></p>
                  </div>
            </div>
        </a> 
        <a href="productDetails.php">
            <div class="shadow-white-box featured four-box-size">
                  <div class="width100 white-bg">
                    <img src="img/product-img.jpg" alt="Product Name" title="Product Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name">Pedigree Dentastix Dog Treats</p>
                        <p class="product-price-width text-overflow slider-product-price left-price">RM2800.00</p>
                        <p class="slider-product-price right-like hover1">
                        	<img src="img/favourite-1.png" class="hover1a like-img width100" alt="Favourite" title="Favourite">
                            <img src="img/favourite-2.png" class="hover1b like-img width100" alt="Favourite" title="Favourite">
                        </p>
                        <div class="clear"></div>
                        <p class="left-rating">5<img src="img/yellow-star.png" class="rating-tiny-star" alt="Rating" title="Rating"></p>
                        <p class="right-sold">1000 <span class="sold-color">Sold</span></p>
                  </div>
            </div>
        </a>                 
        
        
        
        
        
           	
    </div>
</div>
<div class="clear"></div>
<style>
	.animated.slideUp{
		animation:none !important;}
	.animated{
		animation:none !important;}
	.product-a .hover1a{
		display:none !important;}
	.product-a .hover1b{
		display:inline-block !important;}	
</style>
<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>