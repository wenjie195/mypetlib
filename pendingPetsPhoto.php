<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Pending Pets Photo | Mypetslibrary" />
<title>Pending Pets Photo | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance">
	<div class="width100">
		<div class="left-h1-div photo-left-div">
            <h1 class="green-text h1-title">Pending Pets Photo</h1>
            <div class="green-border"></div>
		</div>
        <div class="right-add-div button-right-div">
        	<button class="green-button white-text clean left-download-btn">Download All</button>
            <button class="darkgreen-button white-text clean right-uplaod-btn">Reupload All</button>
        </div>        
        
    </div>
    <div class="clear"></div>
	<div class="width103 border-separation">

        <form>
            <div class="four-box-size"> 	
                <div class="shadow-white-box featured">
                      <div class="width100 white-bg">
                        <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                            <p class="width100 text-overflow slider-product-price">JAN-15-PP</p>
                      </div>
                </div>
                <div class="clean green-button featured-same-button open-confirm">Download</div>
                <div class="clean darkgreen-button featured-same-button open-confirm">Reupload</div>             
            </div>
        </form>

        <form>
            <div class="four-box-size"> 	
                <div class="shadow-white-box featured">
                      <div class="width100 white-bg">
                        <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                            <p class="width100 text-overflow slider-product-price">JAN-16-PP</p>
                      </div>
                </div>

                <div class="clean green-button featured-same-button open-confirm">Download</div>
                <div class="clean darkgreen-button featured-same-button open-confirm">Reupload</div>
            </div>
        </form>

        <form>
            <div class="four-box-size"> 	
                <div class="shadow-white-box featured">
                      <div class="width100 white-bg">
                        <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                            <p class="width100 text-overflow slider-product-price">JAN-17-PP</p>
                      </div>
                </div>

                <div class="clean green-button featured-same-button open-confirm">Download</div>
                <div class="clean darkgreen-button featured-same-button open-confirm">Reupload</div>
                
            </div>
        </form>

        <form>
            <div class="four-box-size"> 	
                <div class="shadow-white-box featured">
                      <div class="width100 white-bg">
                        <img src="img/dog.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Pet Name Pet Name Pet Name Pet Name </p>
                            <p class="width100 text-overflow slider-product-price">JAN-18-PP</p>
                      </div>
                </div>

                <div class="clean green-button featured-same-button open-confirm">Download</div>
                <div class="clean darkgreen-button featured-same-button open-confirm">Reupload</div>
                
            </div>
        </form>
     
        </div>

		<div class="clear"></div>
        </div>        
        <div class="clear"></div>
        




<?php include 'js.php'; ?>
</body>
</html>