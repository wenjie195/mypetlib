<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Blog Title | Mypetslibrary" />
<title>Blog Title | Mypetslibrary</title>
<meta property="og:description" content="Blog 1st Paragraph - Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Blog 1st Paragraph - Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">

<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
 
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<?php include 'userHeader.php'; ?>
 

<div class="width100 blog-big-div overflow min-height menu-distance2">
	<div class="blog-inner-div">
        <img src="img/blog-picture.jpg" class="cover-photo" alt="Blog Title" title="Blog Title"> 
        <h1 class="green-text user-title ow-margin-bottom-0">Blog Title</h1>
        <p class="small-blog-date">3/4/2020</p>
        <p class="article-paragraph">
        	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.
        </p>
        <img src="img/bg.jpg" class="blog-content-img" alt="Blog Title" title="Blog Title">
        <p class="article-paragraph">
        	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.
        </p>
        <img src="img/bg.jpg" class="blog-content-img" alt="Blog Title" title="Blog Title">
        <p class="article-paragraph">
        	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.
        </p>
        <img src="img/bg.jpg" class="blog-content-img" alt="Blog Title" title="Blog Title">
        <p class="article-paragraph">
        	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.
        </p>        
        <img src="img/bg.jpg" class="blog-content-img" alt="Blog Title" title="Blog Title">
        <p class="article-paragraph">
        	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.
        </p>  
        <div class="border-new-div share-div">
        	<h1 class="green-text user-title">Share:</h1>
                <div class="clear"></div>
            	<script async src="https://static.addtoany.com/menu/page.js"></script>

                        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                            <a class="a2a_button_copy_link"></a>
                            <a class="a2a_button_facebook"></a>
                            <a class="a2a_button_twitter"></a>
                            <a class="a2a_button_linkedin"></a>
                            <a class="a2a_button_blogger"></a>
                            <a class="a2a_button_facebook_messenger"></a>
                            <a class="a2a_button_whatsapp"></a>
                            <a class="a2a_button_wechat"></a>
                            <a class="a2a_button_line"></a>
                            <a class="a2a_button_telegram"></a>
                            <!--<a class="a2a_button_print"></a>-->
                        </div>         
        </div>
        <div class="border-new-div share-div">
        	<h1 class="green-text user-title">Recommended for You:</h1>      
        	<div class="width100 recommend">
            	    <a href="pet-blog.php" class="opacity-hover">
                        <div class="shadow-white-box width100 blog-box opacity-hover">
                            <div class="left-img-div2">
                                <img src="img/blog-picture.jpg" class="width100" alt="Blog Title" title="Blog Title">
                            </div>
                            <div class="right-content-div3">
                                <h3 class="article-title text-overflow">
                                    The Standing Trick Tutorial
                                </h3>
                                <p class="date-p">
                                    29/11/2019
                                </p>
                                <p class="right-content-p">
                                   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.
                                </p>
                            </div>
                        </div>
                    </a>    
			</div>
        	<div class="width100 recommend">
            	    <a href="pet-blog.php" class="opacity-hover">
                        <div class="shadow-white-box width100 blog-box opacity-hover">
                            <div class="left-img-div2">
                                <img src="img/blog-picture.jpg" class="width100" alt="Blog Title" title="Blog Title">
                            </div>
                            <div class="right-content-div3">
                                <h3 class="article-title text-overflow">
                                    The Standing Trick Tutorial
                                </h3>
                                <p class="date-p">
                                    29/11/2019
                                </p>
                                <p class="right-content-p">
                                   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.
                                </p>
                            </div>
                        </div>
                    </a>    
			</div>            
            </div>
        </div>
        
                      
	</div>
</div>


<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>