<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Tri Colour Welsh Corgi (Female) Puppy For Sale | Mypetslibrary" />
<title>Tri Colour Welsh Corgi (Female) Puppy For Sale | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary - Tri Colour Welsh Corgi (Female) Puppy For Sale" />
<meta name="description" content="Mypetslibrary - Tri Colour Welsh Corgi (Female) Puppy For Sale" />
<meta name="keywords" content="Tri Colour Welsh Corgi, Puppy For Sale, Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'userHeaderAfterLogin.php'; ?>
<div class="width100 menu-distance3 same-padding min-height2">
	<div class="left-image-div">
       <div class="item">            
            <div class="clearfix">
                <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                    <li data-thumb="img/puppy1.jpg" class="pet-slider-li"> 
                        <img src="img/puppy1.jpg" class="pet-slider-img" alt="Pet Name" title="Pet Name" />
                    </li>
                    <li data-thumb="img/puppy2.jpg" class="pet-slider-li"> 
                        <img src="img/puppy2.jpg" class="pet-slider-img" alt="Pet Name" title="Pet Name"  />
                    </li>
                    <li data-thumb="img/puppy3.jpg" class="pet-slider-li"> 
                        <img src="img/puppy3.jpg" class="pet-slider-img" alt="Pet Name" title="Pet Name"  />
                    </li>
                    <li data-thumb="img/puppy4.jpg" class="pet-slider-li"> 
                        <img src="img/puppy4.jpg" class="pet-slider-img" alt="Pet Name" title="Pet Name"  />
                         </li>
                    <li data-thumb="img/video.jpg" class="pet-slider-li"> 
                        <iframe src="https://player.vimeo.com/video/383039356" class="video-iframe" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Display none or add class hidden if the dog not yet sold -->
        <div class="sold-label">Sold</div>
        
        
    </div>
    
    <div class="right-content-div2">
    	<p class="green-text breed-p">Welsh Corgi</p>
        <h1 class="green-text pet-name">Tri Colour Welsh Corgi (Female) Puppy For Sale</h1>
        <p class="price-p2">RM5XXX</p>
        <div class="right-info-div">
        	<a href="tel:+60383190000" class="contact-icon hover1">
            	<img src="img/call.png" class="hover1a" alt="Call" title="Call">
                <img src="img/call2.png" class="hover1b" alt="Call" title="Call">
            </a>
        	<a class="contact-icon hover1">
            	<img src="img/sms.png" class="hover1a" alt="SMS" title="SMS">
                <img src="img/sms2.png" class="hover1b" alt="SMS" title="SMS">
            </a>  
        	<a class="contact-icon hover1">
            	<img src="img/whatsapp.png" class="hover1a" alt="Whatsapp" title="Whatsapp">
                <img src="img/whatsapp2.png" class="hover1b" alt="Whatsapp" title="Whatsapp">
            </a>  
         	<a class="contact-icon hover1">
            	<img src="img/favourite-1.png" class="hover1a" alt="Favourite" title="Favourite">
                <img src="img/favourite-2.png" class="hover1b" alt="Favourite" title="Favourite">
            </a>  

         	<a class="contact-icon hover1 last-contact-icon open-social">
                    <img src="img/share.png" class="hover1a" alt="Share" title="Share">
                    <img src="img/share2.png" class="hover1b" alt="Share" title="Share">
            </a>
            
        </div>
        <div class="clear"></div>
            <div class="pet-details-div">
            	<table class="pet-table">
                	<tr>
                    	<td class="grey-p">SKU</td>
                        <td class="grey-p">:</td>
						<td>Jan-F-02</td>
                    </tr>
                	<tr>
                    	<td class="grey-p">Gender</td>
                        <td class="grey-p">:</td>
						<td>Female</td>
                    </tr>  
                	<tr>
                    	<td class="grey-p">Age</td>
                        <td class="grey-p">:</td>
						<td>2 months</td>
                    </tr>
                	<tr>
                    	<td class="grey-p">Size</td>
                        <td class="grey-p">:</td>
						<td>Medium</td>
                    </tr>  
                	<tr>
                    	<td class="grey-p">Colour</td>
                        <td class="grey-p">:</td>
						<td>Tri Colour</td>
                    </tr>
                	<tr>
                    	<td class="grey-p">Vaccinated</td>
                        <td class="grey-p">:</td>
						<td>Yes</td>
                    </tr> 
                	<tr>
                    	<td class="grey-p">Dewormed</td>
                        <td class="grey-p">:</td>
						<td>Yes</td>
                    </tr>
                	<tr>
                    	<td class="grey-p">Additional Information</td>
                        <td class="grey-p">:</td>
						<td>With MKA Cert & Microchip</td>
                    </tr>                	
                    <tr>
                    	<td class="grey-p">Published Date</td>
                        <td class="grey-p">:</td>
						<td>06-Jan-20</td>
                    </tr>                    
                    <tr>
                    	<td class="grey-p">Location</td>
                        <td class="grey-p">:</td>
						<td>Georgetown, Penang</td>
                    </tr>                    
                    <tr>
                    	<td class="grey-p">Delivery Service</td>
                        <td class="grey-p">:</td>
						<td>Yes</td>
                    </tr>                                                                                                                                        
                </table>
                
            </div>  
            <h1 class="green-text seller-h1">Seller</h1>
            <div class="clear"></div>
            <div class="seller-profile-div">
            	<a href="petSellerDetails.php" class="opacity-hover"><img src="img/partner2.png" class="seller-profile" alt="Pet Seller" title="Pet Seller"></a>
            </div>
            <div class="right-seller-details">
            	<a href="petSellerDetails.php" class="opacity-hover"><h1 class="seller-name-h1">Fur Fur Fur Fur Fur Fur Fur FUr</h1></a>
                <p class="left-review-p grey-p">Reviews</p>
                <p class="left-review-mark">4/5</p>
                <p class="right-review-star">
                	<img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                    <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                    <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                    <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                    <img src="img/grey-star.png" alt="Review" title="Review" class="star-img last-star">
                </p>
            </div>                        
        </div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>
<div class="sticky-distance2 width100">

</div>
<div class="sticky-call-div">
	<a  href="tel:+60383190000" class="three-div-width text-center sticky-call-style clean transparent-button" >
    	Call
    </a>
	<button class="three-div-width text-center sticky-call-style clean transparent-button">
    	SMS
    </button> 
	<button class="three-div-width text-center sticky-call-style clean transparent-button">
    	Whatsapp
    </button>       
</div>

<?php include 'stickyFooter.php'; ?>
<style>
.social-dropdown {
    width: 360px;
}
</style>
</body>
</html>