<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Pending Pets Details | Mypetslibrary" />
<title>Pending Pets Details | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance">
	<div class="width100">
        <div class="dual-left-div">
            <h1 class="green-text h1-title">Pending Pets Details | <a href="rejectedPets.php" class="green-a">Rejected</a></h1>
            <div class="green-border"></div>
        </div>
        <div class="dual-search-div">
        	<form>
            <input class="line-input clean" type="text" placeholder="Search">
                <button class="search-btn hover1 clean">
                        <img src="img/search.png" class="visible-img hover1a dual-search" alt="Search" title="Search">
                        <img src="img/search2.png" class="visible-img hover1b dual-search" alt="Search" title="Search">
                </button>
            </form>
        </div>

        
    </div>
    <div class="clear"></div>
	<div class="width100 scroll-div border-separation">
    	<table class="green-table width100">
        	<thead>
            	<tr>
                	<th class="first-column">No.</th>
                    <th>Pet Type</th>
                    <th>Pet Name</th>
                    <th>SKU</th>
                    <th>Added On</th>
                    <th>Status</th>
                    <th>Details</th>
                </tr>
            </thead>
            <tbody>
            	<tr>
                	<td class="first-column">1.</td>
                    <td>Puppy</td>
                    <td>Husky</td>
                    <td>FF-A-01</td>
                    <td>1/12/2019</td>
                    <td>Pending</td>
                    <td>
                    	<a href="pendingPetDetails.php" class="hover1">
                        	<img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Approval" title="Approval">
                            <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Approval" title="Approval">
                        </a>
                    </td>
                </tr>
            	<tr>
                	<td class="first-column">2.</td>
                    <td>Puppy</td>
                    <td>Husky</td>
                    <td>FF-A-01</td>
                    <td>1/12/2019</td>
                    <td>Pending</td>
                    <td>
                    	<a href="pendingPetDetails.php" class="hover1">
                        	<img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Approval" title="Approval">
                            <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Approval" title="Approval">
                        </a>
                    </td>
                </tr> 
            	<tr>
                	<td class="first-column">3.</td>
                    <td>Puppy</td>
                    <td>Husky</td>
                    <td>FF-A-01</td>
                    <td>1/12/2019</td>
                    <td>Pending</td>
                    <td>
                    	<a href="pendingPetDetails.php" class="hover1">
                        	<img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Approval" title="Approval">
                            <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Approval" title="Approval">
                        </a>
                    </td>
                </tr>                               
            </tbody>
        </table>
    </div>
    <div class="clear"></div>
    <div class="width100 bottom-spacing"></div>

</div>
<div class="clear"></div>



<?php include 'js.php'; ?>
</body>
</html>