<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Pet Summary | Mypetslibrary" />
<title>Pet Summary | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance">
	<h1 class="green-text h1-title">Pet Summary</h1>
	<div class="green-border"></div>
    <div class="clear"></div>
    <div class="width100 border-separation">
    	<a href="allPuppies.php" class="opacity-hover">
            <div class="white-dropshadow-box four-div-box">
                <img src="img/puppy.png" alt="Puppy" title="Puppy" class="four-div-img">
                <p class="four-div-p">Total Puppies</p>
                <p class="four-div-amount-p"><b>250</b></p>
            </div>
        </a>
        <a href="allKittens.php"  class="opacity-hover">
            <div class="white-dropshadow-box four-div-box second-four-div-box left-four-div">
                <img src="img/kitten.png" alt="Kitten" title="Kitten" class="four-div-img">
                <p class="four-div-p">Total Kittens</p>
                <p class="four-div-amount-p"><b>250</b></p>
            </div> 
        </a>
        <a  href="allReptiles.php" class="opacity-hover">
            <div class="white-dropshadow-box four-div-box right-four-div">
                <img src="img/reptile.png" alt="Reptiles" title="Reptiles" class="four-div-img">
                <p class="four-div-p">Total Reptiles</p>
                <p class="four-div-amount-p"><b>250</b></p>
            </div>  
        </a>
        <a href="pendingPets.php" class="opacity-hover">       
            <div class="white-dropshadow-box four-div-box second-four-div-box forth-div">
                <img src="img/pending-pet.png" alt="Pending Pets Details" title="Pending Pets Details" class="four-div-img">
                <p class="four-div-p">Pending Pets Details</p>
                <p class="four-div-amount-p"><b>10</b></p>
            </div>  
        </a>
        <a href="pendingPetsPhoto.php" class="opacity-hover">       
            <div class="white-dropshadow-box four-div-box">
                <img src="img/pet-photo.png" alt="Pending Pets Photo" title="Pending Pets Photo" class="four-div-img">
                <p class="four-div-p">Pending Pets Photo</p>
                <p class="four-div-amount-p"><b>40</b></p>
            </div> 
        </a>              
    </div>
    <div class="clear"></div>
    <div class="width100 bottom-spacing"></div>

</div>
<div class="clear"></div>



<?php include 'js.php'; ?>
</body>
</html>