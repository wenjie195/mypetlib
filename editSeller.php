<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Seller | Mypetslibrary" />
<title>Edit Seller | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'userHeaderAfterLogin.php'; ?>
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
            <h1 class="green-text h1-title">Edit Sellers</h1>
            <div class="green-border"></div>
   </div>
   <div class="border-separation">
        <div class="clear"></div>
 		<form>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Company Name*</p>
        	<input class="input-name clean input-textarea admin-input" value="" type="text" placeholder="Company Name" required name="">      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Company Slug (For URL)*</p>
        	<input class="input-name clean input-textarea admin-input" value="" type="text" placeholder="Company Slug" required name="">     
        </div>        
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Company Registered No.</p>
        	<input class="input-name clean input-textarea admin-input" value="" type="text" placeholder="Company Registered No." name="">      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Company Contact</p>
        	<input class="input-name clean input-textarea admin-input" value="" type="text" placeholder="Company Contact" name="">    
        </div>        
        <div class="clear"></div>        
        <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Company Address</p>
        	<textarea class="input-name clean input-textarea address-textarea admin-address-textarea" value="" type="text" placeholder="Company Address" name=""></textarea>
        </div>
        <div class="clear"></div>
        
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Company State</p>
        	<select class="input-name clean admin-input" required >
            	<option>State</option>
                <option>Penang</option>
            </select>      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Account Status</p>
        	<select class="input-name clean admin-input" required >
            	<option>Active</option>
                <option>Inactive</option>
            </select>       
        </div>         
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Contact Person Name*</p>
        	<input class="input-name clean input-textarea admin-input" value="" type="text" placeholder="Contact Person Name" required name="">      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Contact Person Contact No.*</p>
        	<input class="input-name clean input-textarea admin-input" value="" type="text" placeholder="Contact No." required name="">    
        </div>         
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Years of Experience</p>
        	<input class="input-name clean input-textarea admin-input" value="" type="text" placeholder="Years of Experience"  name="">      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Cert</p>
        	<input class="input-name clean input-textarea admin-input" value="" type="text" placeholder="Cert"  name="">    
        </div>         
        <div class="clear"></div>
        <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Services</p>
                <div class="filter-option">
                    <label for="puppySellers" class="filter-label filter-label2">Puppy Sellers
                        <input type="checkbox" name="puppySellers" id="puppySellers" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>  
                </div>
                <div class="filter-option">
                    <label for="petHotel" class="filter-label filter-label2">Pet Hotel
                        <input type="checkbox" name="petHotel" id="petHotel" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label> 
                </div>
                <div class="filter-option">                
                    <label for="kittenSellers" class="filter-label filter-label2">Kitten Sellers
                        <input type="checkbox" name="kittenSellers" id="kittenSellers" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>  
                </div>
                <div class="filter-option">    
                    <label for="vet" class="filter-label filter-label2">Vet
                        <input type="checkbox" name="vet" id="vet" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>  
                </div>
                <div class="filter-option">                 
                    <label for="reptileSellers" class="filter-label filter-label2">Reptile Sellers
                        <input type="checkbox" name="reptileSellers" id="reptileSellers" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>                                     	
                </div>
                <div class="filter-option">                 
                    <label for="grooming" class="filter-label filter-label2">Grooming
                        <input type="checkbox" name="grooming" id="grooming" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>                                     	
                </div>
                <div class="filter-option">                 
                    <label for="deliveryServices" class="filter-label filter-label2">Delivery Services
                        <input type="checkbox" name="deliveryServices" id="deliveryServices" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>                                     	
                </div>
                <div class="filter-option">                 
                    <label for="restaurant" class="filter-label filter-label2">Restaurant
                        <input type="checkbox" name="restaurant" id="restaurant" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>                                     	
                </div>
                <div class="filter-option">                 
                    <label for="others" class="filter-label filter-label2">Others
                        <input type="checkbox" name="others" id="others" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>                                     	
                </div>                                        	
        </div>
        <div class="clear"></div>
        <div class="width100 overflow padding-top30">
        	<p class="input-top-p admin-top-p">Type of Breed</p>
        	<input class="input-name clean input-textarea admin-input" value="" type="text" placeholder="Type of Breed"  name="">           			
        </div>
        <div class="clear"></div>
        <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Other Info</p>
        	<input class="input-name clean input-textarea admin-input" value="" type="text" placeholder="Other Info Like FB, Insta Link"  name="">           			
        </div>
        <div class="clear"></div>
		<div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Upload Company Logo</p>
            <!-- Photo cropping into square size feature -->
        </div>       
        <div class="clear"></div>  
        <div class="width100 overflow text-center">     
        	<button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button>
        </div>
        </form>
	</div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>