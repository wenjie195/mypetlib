<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Login | Mypetslibrary" />
<title>Login | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="background-div">
<?php include 'userHeader.php'; ?>
	<div class="width100 same-padding">
        <div class="login-white-box ">
            <h1 class="green-text h1-title login-h1">Admin/Seller Login</h1>
            <div class="green-border"></div>
            <form action="utilities/loginFunction.php" method="POST">
                <input class="clean line-input admin-input" type="text" required placeholder="Username" id="username" name="username">
                <input class="clean line-input admin-input" type="password" required placeholder="Password" id="password" name="password">
                <p class="visible-p hover1">
                	<img src="img/visible.png" class="visible-img hover1a" alt="View Password" title="View Password">
                    <img src="img/visible2.png" class="visible-img hover1b" alt="View Password" title="View Password">
                </p>
                <div class="checkbox-div">
                  <label><input type="checkbox" value="" class="remember-me">Remember Me?</label>
                </div>
                
                <button class="green-button white-text width100 clean2" name="loginButton">
                	Login
                </button>
                <p class="text-center"><a class="open-forgot green-a forgot-a">Forgot Password?</a></p>
            </form>
        </div>
    </div>
</div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Login!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no user with this username ! Please login again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "User registration failed!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Incorrect password! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>