<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add Featured Partners | Mypetslibrary" />
<title>Add Featured Partners | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance  padding-bottom30">
	<div class="width100">
		<div class="left-h1-div featured-left">
            <h1 class="green-text h1-title">Add Featured Partners</h1>
            <div class="green-border"></div>
		</div>        
        
    </div>
    <div class="clear"></div>
    <div class="width100">
    	   <form>
            <input class="line-input clean" type="text" placeholder="Search Partner Name">
                <button class="search-btn hover1 clean">
                        <img src="img/search.png" class="visible-img hover1a" alt="Search" title="Search">
                        <img src="img/search2.png" class="visible-img hover1b" alt="Search" title="Search">
                </button>
            </form>
    </div>
	<div class="width103 border-separation">


        <form>
            <div class="four-box-size"> 	
                <div class="shadow-white-box featured">
                      <div class="width100 white-bg">
                        <img  src="img/pet-seller.jpg" alt="Partner Name" title="Partner Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Partner Name</p>
                            <p class="width100 text-overflow slider-product-price">Penang</p>
                      </div>
                </div>

                <div class="clean red-btn featured-same-button open-confirm">Delete</div>
                            <!-- Double Confirm Modal -->
                            <div id="confirm-modal" class="modal-css">
                            
                              <!-- Modal content -->
                              <div class="modal-content-css confirm-modal-margin">
                                <span class="close-css close-confirm">&times;</span>
                                <div class="clear"></div>
                                <h2 class="green-text h2-title confirm-title">Confirm Delete?</h2>
                                <div class="clean cancel-btn close-confirm">Cancel</div>
                                <button class="clean red-btn delete-btn2">Delete</button>
                                <div class="clear"></div>
                                
                                   
                              </div>
                            
                            </div>                 
            </div>
        </form>

        <form>
            <div class="four-box-size"> 	
                <div class="shadow-white-box featured">
                      <div class="width100 white-bg">
                        <img  src="img/pet-seller.jpg" alt="Partner Name" title="Partner Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Partner Name</p>
                            <p class="width100 text-overflow slider-product-price">Penang</p>
                      </div>
                </div>

                <div class="clean red-btn featured-same-button open-confirm">Delete</div>
                
            </div>
        </form>

        <form>
            <div class="four-box-size"> 	
                <div class="shadow-white-box featured">
                      <div class="width100 white-bg">
                        <img  src="img/pet-seller.jpg" alt="Partner Name" title="Partner Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Partner Name</p>
                            <p class="width100 text-overflow slider-product-price">Penang</p>
                      </div>
                </div>

                <div class="clean red-btn featured-same-button open-confirm">Delete</div>
                
            </div>
        </form>

        <form>
            <div class="four-box-size"> 	
                <div class="shadow-white-box featured">
                      <div class="width100 white-bg">
                        <img  src="img/pet-seller.jpg" alt="Partner Name" title="Partner Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Partner Name</p>
                            <p class="width100 text-overflow slider-product-price">Penang</p>
                      </div>
                </div>

                <div class="clean red-btn featured-same-button open-confirm">Delete</div>
                
            </div>
        </form>

        <form>
            <div class="four-box-size"> 	
                <div class="shadow-white-box featured">
                      <div class="width100 white-bg">
                        <img  src="img/pet-seller.jpg" alt="Partner Name" title="Partner Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Partner Name</p>
                            <p class="width100 text-overflow slider-product-price">Penang</p>
                      </div>
                </div>

                <div class="clean red-btn featured-same-button open-confirm">Delete</div>
                            <!-- Double Confirm Modal -->
                            <div id="confirm-modal" class="modal-css">
                            
                              <!-- Modal content -->
                              <div class="modal-content-css confirm-modal-margin">
                                <span class="close-css close-confirm">&times;</span>
                                <div class="clear"></div>
                                <h2 class="green-text h2-title confirm-title">Confirm Delete?</h2>
                                <div class="clean cancel-btn close-confirm">Cancel</div>
                                <button class="clean red-btn delete-btn2">Delete</button>
                                <div class="clear"></div>
                                
                                   
                              </div>
                            
                            </div>                 
            </div>
        </form>

        <form>
            <div class="four-box-size"> 	
                <div class="shadow-white-box featured">
                      <div class="width100 white-bg">
                        <img  src="img/pet-seller.jpg" alt="Partner Name" title="Partner Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Partner Name</p>
                            <p class="width100 text-overflow slider-product-price">Penang</p>
                      </div>
                </div>

                <div class="clean red-btn featured-same-button open-confirm">Delete</div>
                
            </div>
        </form>

        <form>
            <div class="four-box-size"> 	
                <div class="shadow-white-box featured">
                      <div class="width100 white-bg">
                        <img  src="img/pet-seller.jpg" alt="Partner Name" title="Partner Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Partner Name</p>
                            <p class="width100 text-overflow slider-product-price">Penang</p>
                      </div>
                </div>

                <div class="clean red-btn featured-same-button open-confirm">Delete</div>
                
            </div>
        </form>

        <form>
            <div class="four-box-size"> 	
                <div class="shadow-white-box featured">
                      <div class="width100 white-bg">
                        <img  src="img/pet-seller.jpg" alt="Partner Name" title="Partner Name" class="width100 two-border-radius">
                      </div>
                      <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name">Partner Name</p>
                            <p class="width100 text-overflow slider-product-price">Penang</p>
                      </div>
                </div>

                <div class="clean red-btn featured-same-button open-confirm">Delete</div>
                
            </div>
        </form>               
        </div>

		<div class="clear"></div>
        <div class="width100 overflow text-center">
        	<button class="green-button white-text clean2 edit-1-btn">Save</button>
        </div>
        </div>        
        <div class="clear"></div>
        




<?php include 'js.php'; ?>
</body>
</html>