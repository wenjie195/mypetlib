<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add New Product | Mypetslibrary" />
<title>Add New Product | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'userHeaderAfterLogin.php'; ?>
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
            <h1 class="green-text h1-title">Add New Product</h1>
            <div class="green-border"></div>
   </div>
   <div class="border-separation">
        <div class="clear"></div>
 		<form>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Product Name*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Product Name" required name="">      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">SKU*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="SKU" required name="">     
        </div>        
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Product Slug (for URL, no spacing or contain any symbol except -)</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Product Slug"  required name="">      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Price (RM)*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Price (RM)"  required name="">    
        </div> 
        <div class="clear"></div>  
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Category*</p>
        	<select class="input-name clean admin-input" required >
            	<option>Accessory</option>
                <option>Food</option>
                <option>Grooming</option>
                <option>Health</option>
                <option>Supplies</option>
                <option>Toys</option>
            </select>        
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Brand*</p>
        	<select class="input-name clean admin-input" required >
            	<option>Pedigree</option>
                <option>Pedigree</option>
            </select>   
        </div>           
		<div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">For Animal Type*</p>
        	<select class="input-name clean admin-input" required >
            	<option>Puppy</option>
                <option>Kitten</option>
                <option>Reptile</option>
                <option>Other</option>
            </select>       
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Stock*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Amount" name="" required>  
        </div>  
        
        <div class="clear"></div>
         <div class="dual-input">
        	<p class="input-top-p admin-top-p">Expiry Date*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Expiry Date" name="" required>         
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Status</p>
        	<select class="input-name clean admin-input" required >
            	<option>Available</option>
                <option>Sold</option>
            </select>   
        </div>  
        <div class="clear"></div>
         <div class="dual-input">
        	<p class="input-top-p admin-top-p">Product Description*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Product Description" name="" required>              
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Video Link (Optional)</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Video Link" name="" required>     
        </div>          
        <div class="clear"></div>       

		<div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Upload Pet Photo X4*</p>
            <!-- Photo cropping into square size feature -->
        </div>             

    	

        <div class="clear"></div>  
        <div class="width100 overflow text-center">     
        	<button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button>
        </div>
        </form>
	</div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>