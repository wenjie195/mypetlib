<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Change Password | Mypetslibrary" />
<title>Change Password | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'userHeaderAfterLogin.php'; ?>
	<div class="width100 same-padding overflow min-height menu-distance2">
    	<p class="review-product-name">Change Password</p>
 		<form>
        <div class="dual-input">
        	<p class="input-top-p">Current Password</p>
        	<div class="edit-password-input-div">
                <input class="input-name clean input-password edit-password-input" type="Password" placeholder="Current Password" required name="">   
                <p class="edit-p-password"><img src="img/visible.png" class="hover1a edit-password-img" alt="View Password" title="View Password"><img src="img/visible2.png" class="hover1b edit-password-img" alt="View Password" title="View Password"></p>               
            </div>
            <p class="input-top-p">New Password</p>
            <div class="edit-password-input-div">
            	<input class="input-name clean input-password edit-password-input"  type="Password" placeholder="New Password" required name="">
                <p class="edit-p-password"><img src="img/visible.png" class="hover1a edit-password-img" alt="View Password" title="View Password"><img src="img/visible2.png" class="hover1b edit-password-img" alt="View Password" title="View Password"></p>   
            </div>
            <p class="input-top-p">Retype New Password</p>
            <div class="edit-password-input-div">
            	<input class="input-name clean input-password edit-password-input"  type="Password" placeholder="Retype New Password" required name="">
                <p class="edit-p-password"><img src="img/visible.png" class="hover1a edit-password-img" alt="View Password" title="View Password"><img src="img/visible2.png" class="hover1b edit-password-img" alt="View Password" title="View Password"></p>
            </div>   
        </div>
        <div class="clear"></div>
        	<button class="green-button white-text clean2 edit-1-btn">Submit</button>
        </form>
	</div>
<div class="clear"></div>
<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>