<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Profile | Mypetslibrary" />
<title>Edit Profile | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'userHeaderAfterLogin.php'; ?>
	<div class="width100 same-padding overflow min-height menu-distance2">
    	<p class="review-product-name">Edit Profile</p>
 		<form>
        <div class="dual-input">
        	<p class="input-top-p">Name</p>
        	<input class="input-name clean input-textarea" type="text" placeholder="Name" required name="" value="">   
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p">Gender</p>     
        	<select class="input-name clean" required >
                <option>Female</option>
                <option>Male</option>
            </select> 
        </div>        
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p">Birthday</p>
        	<input type="date"   class="input-name clean" value="">     
        </div>
       
        <div class="clear"></div>
        <div class="width100 overflow text-center">     
        	<button class="blue-button white-text clean2 edit-1-btn margin-auto">Link with Facebook</button>
        </div>       
        <div class="width100 overflow text-center">     
        	<button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button>
        </div>
        </form>
	</div>
<div class="clear"></div>
<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>